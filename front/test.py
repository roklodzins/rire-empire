import datetime

from kivy.clock import Clock
from kivy.lang import Builder

from kivymd.app import MDApp
from kivymd.uix.pickers import MDDatePicker

from math import sin
from kivy_garden.graph import Graph, MeshLinePlot, SmoothLinePlot

KV = '''
MDFloatLayout:

    BoxLayout:
    
        orientation: 'vertical'
            
        BoxLayout:
            size_hint: None, None
            size: 500, 500
            GraphE:
                size_hint: 1, 1
        
        MDRaisedButton:
            text: "Open date picker"
            pos_hint: {'center_x': .5, 'center_y': .5}
            on_release: app.show_date_picker()
'''


class Precision(str):

    @staticmethod
    def norm(s: str) -> str:
        s = s[::-1]
        return " ".join(s[i:i+3] for i in range(0, len(s), 3))[::-1]

    def __mod__(self, u):
        return self.norm(str(int(u))) if int(u) == u else f"{u:.2f}"


class GraphE(Graph):
    def __init__(self, **kwargs):
        super().__init__(**kwargs, xlabel='X', ylabel='Y', x_ticks_minor=2, precision=Precision(),
                         x_ticks_major=5,
                         y_ticks_major=100000,
                         y_grid_label=True, x_grid_label=True, padding=5, border_color=[1, 0, 0, 1],
                         x_grid=True, y_grid=True, xmin=-0, xmax=100, ymin=-1, ymax=1)
        self.plot = SmoothLinePlot(color=[0, 0.7, 0.5, 1])
        self.add_plot(self.plot)
        Clock.schedule_once(self.u, 1)

    def u(self, *_):
        p=self.plot.points = [(0, 196561755), (1, 196563511), (2, 196565267), (3, 196567023), (4, 196568779), (5, 196570534), (6, 196572290),
                       (7, 196574046), (8, 196575802), (9, 196577558), (10, 196579314), (11, 196581070), (12, 196582826), (13, 196584582),
                       (14, 196586338), (15, 196588094), (16, 196589850), (17, 196591606), (18, 196593362), (19, 196595118), (20, 196596874), (21, 196598630), (22, 196600387),
                       (23, 196602143), (24, 196603899), (25, 196605655), (26, 196607411), (27, 196609168), (28, 196610924), (29, 196612680)]
        #self.add_plot(plot)
        m = min(i[1] for i in p)
        M = max(i[1] for i in p)
        if m == M:
            m -= 1
            M += 1
        self.ymin = m
        self.ymax = M
        self.xmin = 0
        self.xmax = len(p)
        self.x_ticks_major = 10
        self.y_ticks_major = max(int((M - m) / 4), 1)


class Test(MDApp):
    def build(self):
        self.theme_cls.theme_style = "Dark"
        self.theme_cls.primary_palette = "Teal"
        return Builder.load_string(KV)

    def on_save(self, instance, value, date_range):
        '''
        Events called when the "OK" dialog box button is clicked.

        :type instance: <kivymd.uix.picker.MDDatePicker object>;
        :param value: selected date;
        :type value: <class 'datetime.date'>;
        :param date_range: list of 'datetime.date' objects in the selected range;
        :type date_range: <class 'list'>;
        '''

        print(instance, value, type(value), date_range, ">>>")

    def on_cancel(self, instance, value):
        '''Events called when the "CANCEL" dialog box button is clicked.'''
        print(instance, value, "<<<<")

    def show_date_picker(self):
        date_dialog = MDDatePicker(year=2010,
                                   month=10,
                                   day=10,
                                   min_date=datetime.date(2009, 10, 10),
                                   max_date=datetime.date(2010, 10, 10),
                                   title_input="Entrez une date",
                                   title="Choisissez une date",
                                   )
        date_dialog.bind(on_save=self.on_save, on_cancel=self.on_cancel)
        date_dialog.open()

if __name__ == '__main__':0
Test().run()
