from __future__ import annotations
import math

from kivy.animation import Animation
from kivy.app import App
from kivy.clock import Clock
from kivy.core.image import Image
from kivy.core.window import Window
from kivy.properties import NumericProperty, BoundedNumericProperty, ReferenceListProperty, ObjectProperty, \
    AliasProperty, BooleanProperty
from kivy.resources import resource_find
from kivy.uix.boxlayout import BoxLayout
from kivymd.uix.navigationrail import MDNavigationRailItem

from ..utils import dtime
from ..data import Country, Coordinate
from .glob import Glob
from .map import Map
from .data import AppData

from kivy3 import Mesh, Material
from kivy3 import Scene, Renderer, PerspectiveCamera


def fp(x):
    if isinstance(x, float):
        return round(x, 2)
    return (round(i, 2) for i in x)


class Projector:

    def __init__(self, earth, x, y, rotation = None):
        self.x = x
        self.y = y
        self.earth: Earth = earth
        distance = -earth.obj.pos.z
        proj_height = 1 / (1 - 1 / distance ** 2) ** 0.5  # get circle conic-proj area height (=width)

        bg_height = 2 * math.tan(math.radians(earth.camera.fov / 4)) * distance  # on screen size of y axis (the visible height)

        x -= earth.center_x
        y -= earth.center_y

        obj_height = proj_height * earth.height / bg_height  # on screen size (half-sphere of r=1) = radius in px

        norm = (x ** 2 + y ** 2) ** 0.5
        h = norm / obj_height  # dist to center in radius unit [0, 1+]

        self.is_out = h > 1
        if self.is_out: return

        h *= proj_height  # proj h to proj-plane field [0, ~+1]

        a = (h / distance) ** 2 + 1
        b = -2 * distance
        c = distance ** 2 - 1

        dt = (b ** 2 - 4 * a * c) ** 0.5
        sx = (-b - dt) / (
                2 * a)  # sphere section: z coord of the plane cutting the sphere (intersect bt ray line & sphere)
        sy = sx * h / distance

        # aim: ret tx, ty coord on texture [0, 1]

        sx -= distance  # normalize circle (center on origin)

        ux, uy = (x / norm * sy, y / norm * sy) if norm else (0, 0)

        # (ux, uy, sx) is the 3d click dot on the sphere (center on origin)

        self.rotation = tuple(rotation or earth.rotation)
        self.rx, self.ry = rotx, roty = math.radians(self.rotation[0]), math.radians(self.rotation[1])

        x0 = ux
        y0 = uy * math.cos(rotx) - sx * math.sin(rotx)
        z0 = uy * math.sin(rotx) + sx * math.cos(rotx)

        x1 = x0 * math.cos(roty) + z0 * math.sin(roty)
        y1 = y0
        z1 = -x0 * math.sin(roty) + z0 * math.cos(roty)

        ux = x1
        uy = y1
        sx = z1

        # print(ux, uy, sx, ux ** 2 + uy ** 2 + sx ** 2, '\t', uy ** 2 + sx ** 2)
        # print()

        pi2 = math.pi * 2

        S = math.asin(uy)  # (math.asin(uy) + 0 + math.pi / 2) % math.pi - math.pi / 2
        a = math.cos(S)

        v = 1 / 2 + S / math.pi

        ax = ux / a
        az = sx / a

        if ax > 1: ax = 1  # Float fix
        if az > 1: az = 1  # Float fix
        if ax < -1: ax = -1  # Float fix
        if az < -1: az = -1  # Float fix

        Ex = math.acos(ax)
        if az < 0: Ex = math.pi * 2 - Ex

        # Ez = math.asin(az)
        # if Ez < 0: Ez += 2 * math.pi
        # if ax < 0: Ez = (math.pi - Ez) % (2 * math.pi)

        # Ex = (Ex - rot_axis) % pi2  # works
        Ex %= pi2
        E = Ex  # must Ex == Ez
        u = E / (2 * math.pi)

        # print(">>>", Ex, v, ax, az)

        self.dot3d = (
            ux,
            uy,
            sx
        )

        # print("&= ", *fp(self.rotation), '\t', *fp(self.dot3d))

        # Texture Coord in [0, 1]
        self.u = u
        self.v = v

        self.coord = Coordinate.from_2d(u, v)
        # print("\n-->", fp(self.coord.latitude), fp(self.coord.longitude))

    def angle(self, other) -> tuple[float, float]:
        """Get the angle (on axis X & Y) between two points/vectors on the sphere (from self to other)"""

        if self.is_out or other.is_out: return 0, 0
        return self.coord.latitude - other.coord.latitude, other.coord.longitude - self.coord.longitude


class Earth(Renderer, AppData):

    zoom_max = 20
    _zoom = BoundedNumericProperty(0, min=0, max=zoom_max, errorhandler=lambda x: 0 if x < 0 else Earth.zoom_max)
    zoom = AliasProperty(lambda x: x._zoom, lambda x, y: x.update_zoom(y))
    hover_country = NumericProperty(None, allownone=True)

    enable_alliance = BooleanProperty(False)
    selected_war = NumericProperty(None, allownone=True)

    rotation_x = BoundedNumericProperty(0, min=-90, max=90, errorhandler=lambda x: -90 if x < -90 else 90)
    rotation_y = BoundedNumericProperty(0, min=0, max=360, errorhandler=lambda x: x % 360)
    rotation = ReferenceListProperty(rotation_x, rotation_y)

    def __init__(self, **kw):
        super().__init__(shader_file=resource_find("shader.glsl"), **kw)
        self.space_tex = Image(resource_find("Space.jpg")).texture
        self.set_clear_color((.2, .2, .2, 1.))
        self.scene = Scene()
        self.glob = Glob(1, 100, 100)
        self.space = Glob(50, 100, 100, reverse=True)
        self.material = Material(color=(.2, .2, .2), diffuse=(1, 1, 1), specular=(.6, .6, .6), shininess=10)
        self.space_mat = Material(color=(1., 1., 1.), diffuse=(1., 1., 1.), specular=(1, 1, 1))

        self.rendered_map = Map()

        self.rendered_map.draw()
        self.material.map = self.rendered_map.texture
        self.space_mat.map = self.space_tex

        self.obj = Mesh(self.glob, self.material)
        self.space_obj = Mesh(self.space, self.space_mat)

        self.obj.pos.z = -6
        self.space_obj.pos.z = -6

        self.mesh = self.obj._mesh

        self.rotation_anim = Animation(rotation_x=20, d=20, t="in_out_sine") + Animation(rotation_x=-20, d=20, t="in_out_sine")
        self.rotation_anim.repeat = True

        t = Animation(rotation_y=359, d=60, t="linear") + Animation(rotation_y=361, d=1/360, t="linear")
        t.repeat = True
        self.rotation_anim &= t
        self.rotation_anim.start(self)

        self.zoom_anim = None
        self.zoom_target = 0

        self.camera = PerspectiveCamera(45, 0.3, .1, 1000)

        self.scene.add(self.obj)
        self.scene.add(self.space_obj)
        self.main_light.pos = (-3, 3, 3)
        self.render(self.scene, self.camera)

        self.bind(size=self._adjust_aspect)

        self.clicked_point = None
        self.current_point = None
        Window.bind(mouse_pos=self.on_motion)

    def focus(self, coord: Coordinate = None):
        if coord is None:
            if self.selected_country is None: return
            coord = Country[self.selected_country].capcoord

        if self.zoom_anim: self.zoom_anim.cancel(self)
        self.zoom_anim = Animation(_zoom=max(self._zoom - 10, 0), d=0.3, t="out_cubic") + Animation(_zoom=max(self.zoom_max / 2, self.zoom), d=0.3, t="in_cubic")
        self.zoom_anim.start(self)

        if self.rotation_anim: self.rotation_anim.cancel(self)

        ry = 90 - coord.longitude  # dst
        cy = self.rotation_y       # src
        if abs(ry - cy) > abs(ry + 360 - cy):
            ry += 360
        elif abs(ry - cy) > abs(ry - 360 - cy):
            ry -= 360

        self.rotation_anim = Animation(rotation_x=coord.latitude, rotation_y=ry, d=0.5, t="out_cubic")
        self.rotation_anim.start(self)

    def _adjust_aspect(self, inst, val):
        self.camera.aspect = self.width / self.height

    def on_enable_alliance(self, inst, val):
        self.rendered_map.alliance = val
        self.rendered_map.draw()
        self.fbo.ask_update()
        self.fbo.draw()

    def on_selected_war(self, inst, val):
        self.rendered_map.war = val
        self.rendered_map.draw()
        self.fbo.ask_update()
        self.fbo.draw()

    def on_world(self, inst, val):
        self.rendered_map.world = val
        self.rendered_map.draw()
        self.fbo.ask_update()
        self.fbo.draw()

    def on_selected_country(self, inst, val):
        super().on_selected_country(inst, val)
        if val != self.rendered_map.selected_country:
            self.rendered_map.selected_country = val
            self.rendered_map.draw()
            self.fbo.ask_update()
            self.fbo.draw()
            self.focus()

    def on_hover_country(self, inst, val):
        if self.rendered_map.hover_country != val:
            self.rendered_map.hover_country = val
            self.rendered_map.draw()
            self.fbo.ask_update()
            self.fbo.draw()

    def update_zoom(self, val):
        self.zoom_target = val
        if self.zoom_anim: self.zoom_anim.cancel(self)
        self.zoom_anim = Animation(_zoom=val, d=0.1, t="linear")
        self.zoom_anim.start(self)

    def on__zoom(self, inst, val):
        z = val / self.zoom_max
        m = -6
        M = -1.5
        a = M - m
        self.space_obj.pos.z = self.obj.pos.z = -6 + a * z
        self.rendered_map.frontiers = z > .6
        self.rendered_map.draw()
        self.fbo.ask_update()
        self.fbo.draw()

    def on_rotation_x(self, inst, val):
        self.obj.rotation.x = self.space_obj.rotation.x = val
        self.fbo.ask_update()
        self.fbo.draw()

    def on_rotation_y(self, inst, val):
        self.obj.rotation.y = self.space_obj.rotation.y = val
        self.fbo.ask_update()
        self.fbo.draw()

    def on_rotation(self, inst, val):
        pass  # print("rot", val)

    def on_touch_down(self, touch):
        # check collision with touch
        if not self.collide_point(*touch.pos):
            return False

        match touch.button:
            case "scrollup":
                self.zoom = self.zoom_target - 1
            case "scrolldown":
                self.zoom = self.zoom_target + 1
            case "left":
                if self.rotation_anim: self.rotation_anim.cancel(self)
                self.current_point = self.clicked_point = Projector(self, *touch.pos)
            # case "right":
            #     # save map to png
            #     print("DEBUG: saving rendered map... no")
            #     self.texture.save("test.png")

        return True

    def on_touch_up(self, touch):
        if not self.collide_point(*touch.pos):
            return False

        match touch.button:
            case "left":
                if self.clicked_point is not self.current_point:  # moved between down & up
                    self.slide(touch.pos, True)
                else:  # simple click
                    self.selected_country = self.hover_country

                self.clicked_point = self.current_point = None

    def on_touch_move(self, touch):
        match touch.button:
            case "left":
                self.slide(touch.pos)

    @dtime
    def on_motion(self, w, pos):
        if not self.collide_point(*pos):
            self.hover_country = None
            return

        if self.clicked_point is None:
            u = Projector(self, *pos)

            if not u.is_out:
                u = Coordinate.from_2d(u.u, u.v)

                if self.hover_country is not None:
                    if u in Country[self.hover_country]:
                        return

                for i in Country:
                    i: Country
                    if u in i:
                        # print("set to hover", i.id, i.name)
                        self.hover_country = i.id
                        return

            # print("set to hover", None)
            self.hover_country = None

    def slide(self, pos, last: bool = False):
        if self.clicked_point is None or self.clicked_point.is_out: return

        n = Projector(self, *pos, self.clicked_point.rotation)

        if n.is_out:
            cx, cy = self.rotation
            rx, ry = self.clicked_point.angle(self.current_point)
            rx = min(30, max(-30, rx))
            ry = min(30, max(-30, ry))
        elif last:
            cx, cy = self.rotation
            # rx, ry = self.current_point.angle(n)  # better but always 0...
            rx, ry = self.clicked_point.angle(self.current_point)
            rx = min(10, max(-10, rx / 5))
            ry = min(10, max(-10, ry / 5))
        else:
            cx, cy = self.clicked_point.rotation
            rx, ry = self.clicked_point.angle(n)
            self.rotation = cx + rx, cy + ry
            self.current_point = n
            return

        if self.rotation_anim: self.rotation_anim.cancel(self)
        self.rotation_anim = Animation(rotation_x=cx + rx, rotation_y=cy + ry, d=1, t="out_cubic")
        self.rotation_anim.start(self)
        self.current_point = self.clicked_point = None
