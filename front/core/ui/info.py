from kivy.animation import Animation
from kivy.properties import StringProperty, ObjectProperty, ColorProperty
from kivy.uix.widget import Widget
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.tooltip import MDTooltip
from ..data import Ideology

__all__ = (
    "IdeologyBar",
)


class IdeologyItem(Widget, MDTooltip):
    color = ColorProperty((.1, .1, .1, 1))
    name = StringProperty("")

    def on_hover_visible(self, w, v):  # Fix Hover failing in scrollview
        if not self.hover_visible:
            self.hover_visible = self.hovering


class IdeologyAltItem(IdeologyItem):
    def on_hover_visible(self, w, v):  # Dirty way to disable tooptip
        self.hover_visible = False


class IdeologyBar(MDBoxLayout):
    ideology = ObjectProperty(None, allownone=True)

    COLOR = (
        (235 / 255, 51 / 255, 27 / 255, 1),
        (235 / 255, 76 / 255, 28 / 255, 1),
        (235 / 255, 180 / 255, 14 / 255, 1),
        (7 / 255, 154 / 255, 235 / 255, 1),
        (9 / 255, 6 / 255, 235 / 255, 1),
        (84 / 255, 6 / 255, 235 / 255, 1),
        (128 / 255, 9 / 255, 235 / 255, 1),
    )

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.ideo = []
        for n, i in enumerate(Ideology.names.values()):
            item = IdeologyItem(name=i, color=self.COLOR[n])
            self.ideo.append(item)
            self.add_widget(item)
        self.ideo_alt = IdeologyAltItem()
        self.add_widget(self.ideo_alt)

    def on_ideology(self, w, ideology):
        if ideology is not None:
            for n, i in enumerate(ideology):
                Animation(size_hint_x=i, d=0.5, t="out_cubic").start(self.ideo[n])
            Animation(size_hint_x=0, d=0.5, t="out_cubic").start(self.ideo_alt)
        else:
            for i in self.ideo:
                Animation(size_hint_x=0, d=0.5, t="out_cubic").start(i)
            Animation(size_hint_x=1, d=0.5, t="out_cubic").start(self.ideo_alt)
