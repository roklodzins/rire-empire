import math

from kivy3 import Face3
from kivy3.extras.geometries import SphereGeometry


class Glob(SphereGeometry):

    def __init__(self, rad, sectors, stacks, reverse=False, **kw):
        self.reverse = reverse
        super().__init__(rad, sectors, stacks, **kw)

    def _build_sphere(self):  # override to add UV texture coord
        # Create Vertices and normals + UV
        _vertices = []
        _normals = []
        self.uvs = {}

        # Generate the faces

        sectorStep = 2 * math.pi / self.sectors  # algo from http://www.songho.ca/opengl/gl_sphere.html
        stackStep = math.pi / self.stacks

        for i in range(self.stacks + 1):
            stackAngle = math.pi / 2 - i * stackStep
            xz = math.cos(stackAngle)
            y = math.sin(stackAngle)

            for j in range(self.sectors + 1):
                sectorAngle = j * sectorStep
                x = xz * math.cos(sectorAngle)
                z = xz * math.sin(sectorAngle)

                vertex = (x * self.rad, y * self.rad, z * self.rad)
                _vertices.append(vertex)
                _normals.append((x, y, z))
                u = 1.0 - (float(j) / self.sectors)
                v = 1.0 - (float(i) / self.stacks)

                self.uvs[_vertices[-1]] = (u, v)

        # Generate the Faces with mapping to the vertices

        for i in range(self.stacks):
            k1 = i * (self.sectors + 1)
            k2 = k1 + self.sectors + 1
            for j in range(self.sectors):

                if i:
                    self.faces.append(f := Face3(k1, k2, k1 + 1))
                    f.vertex_normals = [_normals[k1], _normals[k2], _normals[k1 + 1]]

                if i != self.stacks - 1:
                    self.faces.append(f := Face3(k1 + 1, k2, k2 + 1))
                    f.vertex_normals = [_normals[k1 + 1], _normals[k2], _normals[k2 + 1]]

                k1 += 1
                k2 += 1

        self.vertices = _vertices

        if self.reverse:
            for i in self.faces:
                i.vertex_normals = [tuple(-n for n in u) for u in i.vertex_normals]

        def _getv(idx):
            f = self.faces[idx // 3]
            v = [f.a, f.b, f.c][idx % 3]
            return self.vertices[v]

        self.face_vertex_uvs = [type("_", (object,), {"__getitem__": lambda s, idx: self.uvs[_getv(idx)]})()]
