from kivy.core.image import Image
from kivy.graphics import Fbo, Rectangle, Line, Color, Mesh, Callback, ClearColor, ClearBuffers
from kivy.graphics.opengl import glLineWidth
from kivy.graphics.tesselator import Tesselator
from kivy.resources import resource_find

from ..data import Country, Territory, Frontier, WorldState
import warnings as w


class Map(Fbo):  # FBO is not kivy dispatchable -> no kivy properties

    def __init__(self, frontiers=False, **kwargs):
        print("Loading Earth Texture...")
        self.background = Image(resource_find("ESU.png"))
        print("Earth Texture loading done!")
        self._fron_cache = {}
        self._mesh_cache = {}
        self._frontiers = frontiers
        self._selected_country = None
        self._hover_country = None

        self._world: WorldState = None
        self._alliance = False
        self._war = None

        self._ready = False

        print("Generating Main Map Framebuffer...")
        super().__init__(size=self.background.size, **kwargs)
        print("Generating Frontier Framebuffer...")
        self.frontiers_fbo = Fbo(size=self.size)
        print("Generating Alliances Framebuffer...")
        self.alliances_fbo = Fbo(size=self.size)
        print("Generating Wars Framebuffer...")
        self.wars_fbo = Fbo(size=self.size)
        a, b = self.size
        self.rsz = lambda x, y: (a * x, b * y)

        self.add_reload_observer(self._reset)
        self._reset()

        self.frontiers_fbo.add_reload_observer(self._render_frontiers_fbo)
        self.alliances_fbo.add_reload_observer(self._render_alliances_fbo())
        self.wars_fbo.add_reload_observer(self._render_wars_fbo())
        self._render_frontiers_fbo()

        self.mesh_caching()
        self._ready = True

        # def _load(*args):
        #    self.mesh_caching()
        #    self._ready = True
        #
        # Thread(target=_load).start()

    @property
    def ready(self):
        return self._ready

    @property
    def world(self):
        return self._world

    @world.setter
    def world(self, value):
        if value != self._world:
            self._world = value
            self._render_alliances_fbo()
            self._draw()

    @property
    def alliance(self):
        return self._alliance

    @alliance.setter
    def alliance(self, value):
        if value != self._alliance:
            self._alliance = value
            self._draw()

    @property
    def war(self):
        return self._war

    @war.setter
    def war(self, value):
        if value != self._war:
            self._war = value
            self._render_wars_fbo()
            self._draw()

    @property
    def frontiers(self):
        return self._frontiers

    @frontiers.setter
    def frontiers(self, value):
        if value != self._frontiers:
            self._frontiers = value
            self._draw()

    @property
    def selected_country(self):
        return self._selected_country

    @selected_country.setter
    def selected_country(self, value):
        if value != self._selected_country:
            self._selected_country = value
            self._draw()

    @property
    def hover_country(self):
        return self._hover_country

    @hover_country.setter
    def hover_country(self, value):
        if value != self._hover_country:
            self._hover_country = value
            self._draw()



    def _reset(self):
        self.before.clear()

        with self.before:
            Rectangle(size=self.size, texture=self.background.texture)

        self._draw()


    def _draw(self):
        self.clear()

        with self:
            if self._frontiers:
                Rectangle(size=self.size, texture=self.frontiers_fbo.texture)

            if self._world is not None:
                if self._war is not None:
                    Rectangle(size=self.size, texture=self.wars_fbo.texture)

                elif self._alliance:
                    Rectangle(size=self.size, texture=self.alliances_fbo.texture)


        if self._selected_country is not None:
            self.add(Color(0, 1, 1, 0.4))
            self.fill_zone(Country[self._selected_country])

        if self._hover_country is not None and self._hover_country != self._selected_country:
            self.add(Color(.8, .8, .8, 0.3))
            self.fill_zone(Country[self._hover_country])

    def _render_alliances_fbo(self):
        with self.alliances_fbo:
            ClearColor(0, 0, 0, 0)
            ClearBuffers()
        # self.alliances_fbo.clear()
        # self.alliances_fbo.bind()
        # self.alliances_fbo.clear_buffer()
        # self.alliances_fbo.release()

        if self._world is not None:
            with self.alliances_fbo:
                for i in self._world.alliances:
                    Color(*i.color, 0.5)
                    for j in i:
                        self.fill_zone(j, self.alliances_fbo)

        self.alliances_fbo.draw()

    def _render_wars_fbo(self):
        with self.wars_fbo:
            ClearColor(0, 0, 0, 0)
            ClearBuffers()
        # self.wars_fbo.clear()
        # self.wars_fbo.bind()
        # self.wars_fbo.clear_buffer()
        # self.wars_fbo.release()

        if self._war is not None and self._world is not None:

            try:
                w = self._world.wars[self._war]
            except IndexError:
                pass
            else:
                with self.wars_fbo:
                    Color(0.91, 0.259, 0.137, .4)
                    for j in w.attackers_countries:
                        self.fill_zone(j, self.wars_fbo)

                    Color(0.137, 0.522, 0.91, .4)
                    for j in w.defenders_countries:
                        self.fill_zone(j, self.wars_fbo)

        self.wars_fbo.draw()

    def _render_frontiers_fbo(self):
        self.frontiers_fbo.clear()

        with self.frontiers_fbo:
            Color(1, 1, 1, 1)
            Callback(lambda *_: glLineWidth(3))
            for i in Territory:
                i: Territory
                for u in i:
                    self.render_frontiers(u)
            Callback(lambda *_: glLineWidth(1))

        self.frontiers_fbo.draw()

    def render_frontiers(self, frontier: Frontier):
        Line(points=self._get_frontiers(frontier))

    def _get_frontiers(self, frontier: Frontier):
        if fron := self._fron_cache.get(frontier.id):
            return fron
        else:
            i = tuple(c for u in frontier for c in self.rsz(*u.coord2d))
            self._fron_cache[frontier.id] = i
            return i

    def fill_zone(self, zone: Country | Territory, dst: Fbo = None):
        """Fill a zone with the current color
        Args:
            zone (Country | Territory): the zone to fill
            dst (Fbo): the destination Fbo
        """
        if isinstance(zone, Territory):
            zone = [zone]

        if dst is None:
            dst = self

        for territory in zone:
            for i in self._mesh_cache[territory.id]:
                dst.add(i)

    def mesh_caching(self):
        """Caches the meshes of all territories"""
        print("Mesh Caching...")
        x = len(Territory) // 10

        for y, territory in enumerate(Territory):
            territory: Territory
            if y % x == 0: print(f"{int(y / x * 10)}%")
            self._mesh_caching(territory)


    def _mesh_caching(self, territory: Territory):
        """Caches the mesh of a territory
            Args:
                territory (Territory): the territory to cache
        """
        tess = Tesselator()
        o = tuple(c for u in territory.outer for c in self.rsz(*u.coord2d))
        i = tuple(tuple(c for u in inner for c in self.rsz(*u.coord2d)) for inner in territory.inners)
        tess.add_contour(o)

        for n in i: tess.add_contour(n)

        if not tess.tesselate():
            w.warn(f"Tesselation failed with Territory {territory.id}")

        m = self._mesh_cache[territory.id] = tuple(
            Mesh(vertices=vertices, indices=indices, mode="triangle_fan") for vertices, indices in tess.meshes)
        return m
