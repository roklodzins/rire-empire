import random
from PIL import Image
import json
import os
import shutil, logging
logging.getLogger('PIL').setLevel(logging.WARNING)

repertoire_script = os.path.dirname(os.path.abspath(__file__))


class FileManager:
    data_path = os.path.join(repertoire_script, "flag_data")
    data_zip_path = os.path.join(repertoire_script, "flag_data.zip")

    @staticmethod
    def lire_fichier_json(nom_fichier):
        try:
            with open(nom_fichier, 'r') as fichier:
                contenu = json.load(fichier)
            return contenu
        except json.decoder.JSONDecodeError:
            print(f"Erreur de décodage JSON dans le fichier {nom_fichier}. Assurez-vous qu'il n'est pas vide.")
            return None
        except FileNotFoundError:
            print(f"Le fichier {nom_fichier} n'a pas été trouvé.")
            return None

    @staticmethod
    def read_colors(path):
        with open(path, 'r') as file:
            tableau_2d = [line.strip().split() for line in file.readlines()]

        max_len = max(len(line) for line in tableau_2d)
        tableau_tuples = []

        for line in tableau_2d:
            line = line + [''] * (max_len - len(line))
            tableau_tuples.append(tuple(map(int, line)))

        return tableau_tuples

    @staticmethod
    def change_color(image, color):
        image = image.convert('RGBA')
        pixels = image.load()
        for x in range(image.width):
            for y in range(image.height):
                r, g, b, a = pixels[x, y]
                if a > 0:
                    pixels[x, y] = color + (a,)
        return image

    @staticmethod
    def overlap_images(image_1, image_2):
        if image_1.size != image_2.size:
            raise ValueError("The two images must have the same size")
        return Image.alpha_composite(image_1.convert('RGBA'), image_2.convert('RGBA'))

    @staticmethod
    def tableau_vers_dictionnaire(tableau):
        dictionnaire = {}
        for ligne in tableau:
            nom_pays = ligne[0]
            valeurs = [valeur for valeur in ligne[1:] if valeur != '']
            dictionnaire[nom_pays] = valeurs
        return dictionnaire

    @staticmethod
    def charger_donnees(data_path, zip_path):
        data_path = os.path.join(repertoire_script, data_path)
        zip_path = os.path.join(repertoire_script, zip_path)
        if not os.path.exists(data_path):
            print("Données non initialisées, recherche du dossier compressé...")
            if os.path.exists(zip_path):
                shutil.unpack_archive(zip_path, data_path, 'zip')
                print("données drapeaux chargées")
            else:
                print("données drapeaux maquantes")

    @staticmethod
    def serialize(in_path, out_path):
        in_path = os.path.join(repertoire_script, in_path)
        out_path = os.path.join(repertoire_script, out_path)
        shutil.make_archive(out_path.replace('.zip', ''), 'zip', in_path)
        print(f"{in_path} compressé dans {out_path}")


class Color:
    Colors_neutral = {
        "BLACK": (0, 0, 0),
        "WHITE": (255, 255, 255),
        "RED": (255, 0, 0),
        "DARK_RED": (200, 0, 0),
        "BLUE": (0, 38, 255),
        "DARK_BLUE": (0, 38, 100),
        "CYAN": (51, 181, 255),
        "YELLOW": (255, 206, 0),
        "GREEN": (0, 125, 38),
        "ORANGE": (255, 103, 31)}


class Flag:
    image_vide = Image.new('RGBA', (300, 200), (0, 0, 0, 0))
    FileManager.charger_donnees("flag_data", "flag_data.zip")
    layout_json = FileManager.lire_fichier_json(os.path.join(repertoire_script, "flag_data/layout_1966.json"))
    flags_path = f"flag_list"

    def __init__(self, name, ideologie):
        # self.init_all_flags()
        self.name = name
        self.layout = Flag.layout_json[name]

        if self.layout['ideologie'] == ideologie:
            self.ideologie = "Neutre"
        elif ideologie not in ["Neutre", "Monarchie", "Communisme", "Totalitarisme"]:
            print("ERREUR, merci de choisir l'ideologie parmi : Neutre, Monarchie, Communisme, Totalitarisme")

        self.palette = Color.Colors_neutral
        self.ideologie = ideologie
        self.path = os.path.join(repertoire_script, f"{self.flags_path}/{name}.png")

        self.generate_flag()

    def get_path(self):
        return f"flags/{Flag.flags_path}/{self.name}.png"

    @staticmethod
    def init_all_flags(ideologie = "Neutre"):
        print("All Flags Init")
        for country in Flag.layout_json:
            if ideologie == "Neutre":
                Flag(country, Flag.layout_json[country]['ideologie'])
            else:
                Flag(country, ideologie)
        print(f"{len(Flag.layout_json)} drapeaux générés")

    def add_all_layout(self, image, flag):
        symbol_prim = [random.randint(1, 6)]
        symbol_sec = [7, 8, 9, 10]
        random.shuffle(symbol_sec)
        id_symbol = symbol_prim + symbol_sec

        for i in range(len(flag["layout"])):
            layout_path = f"flag_data/flag/{flag['layout'][i]}.png"
            layout_color = self.palette[flag['colors'][i]]
            image = self.add_layout(image, layout_path, layout_color)

        if self.ideologie == "Neutre":
            if "symbol_default" in flag:
                for symbol in flag["symbol_default"]:
                    if "color" in symbol:
                        image = self.add_layout(image,
                                                f"flag_data/symbol/{symbol['layout']}.png",
                                                self.palette[
                                                    symbol["color"][random.randint(0, len(symbol['color']) - 1)]])
                    else:
                        image = self.add_layout(image,
                                                f"flag_data/symbol/{symbol['layout']}.png")
        else:
            if "symbol" in flag:
                for symbol in flag["symbol"][random.randint(0, len(flag["symbol"]) - 1)]:

                    if "mask" in symbol:
                        image = self.add_symbol(image,
                                                "flag_data/symbol/mask.png",
                                                symbol["x"],
                                                symbol["y"],
                                                symbol["size"] + 10,
                                                self.palette[symbol['mask']])

                    if 'id' in symbol:
                        path = f"flag_data/symbol/_Ideologie/{self.ideologie}/{id_symbol[symbol['id'] - 1]}.png"
                    else:
                        path = f"flag_data/symbol/_Ideologie/{self.ideologie}/{random.randint(1, 10)}.png"

                    image = self.add_symbol(image,
                                            path,
                                            symbol["x"],
                                            symbol["y"],
                                            symbol["size"],
                                            self.palette[symbol["color"][random.randint(0, len(
                                                symbol['color']) - 1)]] if 'color' in symbol else None)

        return image

    @staticmethod
    def add_symbol(main_image, symbol_path, x, y, size, color=0):
        symbol_path = os.path.join(repertoire_script, symbol_path)
        symbol = Image.open(symbol_path)
        symbol = symbol.convert("RGBA")
        symbol = symbol.resize((size, size))

        x_position = x - size // 2
        y_position = y - size // 2

        overlay = Image.new("RGBA", main_image.size, (0, 0, 0, 0))

        overlay.paste(symbol, (x_position, y_position), symbol)

        symbol.close()

        if color:
            overlay = FileManager.change_color(overlay, color)

        main_image = Image.alpha_composite(main_image.convert("RGBA"), overlay)

        return main_image

    @staticmethod
    def combine_flags(image_1, image_2, resize_factor=0.5):
        new_size = (int(image_2.width * resize_factor), int(image_2.height * resize_factor))
        image_2 = image_2.resize(new_size)

        combined_image = Image.new('RGBA', image_1.size)
        combined_image.paste(image_1, (0, 0))
        combined_image.paste(image_2, (0, 0), image_2)

        image_1.close()
        image_2.close()

        return combined_image

    @staticmethod
    def add_layout(final_image, path, color=0):
        path = os.path.join(repertoire_script, path)
        nom_image = path
        image_add = Image.open(nom_image)
        if color:
            image_add = FileManager.change_color(image_add, color)
        final_image = FileManager.overlap_images(final_image, image_add)
        image_add.close()
        return final_image

    def generate_flag(self):
        final_image = Flag.image_vide
        indexed_image = Flag.image_vide

        final_image = self.add_all_layout(final_image, self.layout)

        if "indexed" in self.layout:
            indexed_image = self.add_all_layout(indexed_image, self.layout['indexed'])
            final_image = self.combine_flags(final_image, indexed_image)
            indexed_image.close()

        os.path.dirname(self.path)

        if not os.path.exists(os.path.dirname(self.path)):
            os.makedirs(os.path.dirname(self.path))

        final_image.save(self.path)
        final_image.close()
