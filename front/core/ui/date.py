from os.path import isfile
from threading import Thread

from kivy.clock import Clock
from kivy.properties import BooleanProperty, ObjectProperty, StringProperty
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.dialog import MDDialog
from kivymd.uix.pickers import MDDatePicker
from kivymd.uix.button import MDTextButton, MDRectangleFlatIconButton, MDFlatButton

from .data import AppData

__all__ = (
    "DateTimeButton",
)


class SimuConfigContent(MDBoxLayout):

    def check_seed(self, *_):
        try:
            int(self.ids.seed.text)
            self.ids.seed.error = False
            return True
        except:
            self.ids.seed.error = True
            return False

    def check_days(self, *_):
        try:
            int(self.ids.days.text)
            self.ids.days.error = False
            return True
        except:
            self.ids.days.error = True
            return False

    def is_valid(self):
        return self.check_days() and self.check_seed()

    def get(self):
        return {
            "seed": int(self.ids.seed.text),
            "max_days": int(self.ids.days.text),
        }


class SimuDialog(MDDialog):
    mgr = ObjectProperty(None)

    def __init__(self, **kwargs):
        self.config = SimuConfigContent()
        x = kwargs["mgr"]
        super().__init__(**kwargs,
                         title="Configurez la simulation",
                         type="custom",
                         content_cls=self.config,
                         buttons=[
                             MDFlatButton(
                                 text="Annuler",
                                 theme_text_color="Custom",
                                 text_color=x.theme_cls.primary_color,
                                 on_release=lambda *x: self.dismiss(),
                             ),
                             MDFlatButton(
                                 text="Lancer",
                                 theme_text_color="Custom",
                                 text_color=x.theme_cls.primary_color,
                                 on_release=lambda *x: self.validate(),
                             ),
                         ],
                         )

    def validate(self):
        if self.config.is_valid():
            self.dismiss()
            self.mgr.start(**self.config.get())


class DateTimeButton(MDRectangleFlatIconButton, AppData):
    simu_state = StringProperty("wait")
    target = ObjectProperty(None)
    thread_running = BooleanProperty(False)

    def on_save(self, instance, value, date_range):
        self.target = value

        if self.simu.is_loaded(value, update=True):
            self.icon = "calendar-range"
            self.text = self.simu.current_date.strftime("%d/%m/%Y")
            self.update_state()
            return

        r, d = self.simu.is_loadable(value)
        if r:
            self.thread_running = True
            Thread(target=self.set_date).start()
        else:
            self.error("Date invalide", "La date choisie est au delà de la limite courante: " + d.strftime("%d/%m/%Y"))

    def on_cancel(self, instance, value):
        pass

    def start(self, seed: int, max_days: int):
        if not self.simu.start(seed, max_days):
            self.error("Erreur", "Impossible de lancer la simulation, simulateur Escalation introuvable.")
            return
        else:
            self.simu_state = "run"
            self.icon = "download"
            self.text = "Charger"

    def on_release(self):
        if self.thread_running: return

        if self.simu_state == "wait":
            SimuDialog(mgr=self).open()
        elif self.simu_state == "run":
            s = self.simu.init()
            if s:
                self.update_state()
                self.simu_state = "ready"
                self.icon = "calendar-range"
                self.text = self.simu.start_date.strftime("%d/%m/%Y")
            else:
                self.error("Erreur", "Impossible de charger les données de la simulation.")
        else:
            self.date_picker()

    def set_date(self):
        self.simu.load(self.target)
        Clock.schedule_once(lambda *_: setattr(self, "thread_running", False), -1)

    def on_thread_running(self, *_):
        if self.thread_running:
            self.icon = "sync"
            self.text = "Chargement..."
        else:
            self.update_state()
            self.icon = "calendar-range"
            self.text = self.simu.current_date.strftime("%d/%m/%Y")
            if self.simu.current_date != self.target:
                self.error("Données manquantes",
                           f"Impossible de charger les données jusqu'au {self.target.strftime('%d/%m/%Y')}.\nArrêt à la date {self.simu.current_date.strftime('%d/%m/%Y')}.")

    def date_picker(self):
        e = self.simu.end_loadable_date
        if e <= self.simu.start_date:
            self.error("Aucune données", "Aucune données n'est disponible.")
            return
        date_dialog = MDDatePicker(
            year=self.simu.current_date.year,
            month=self.simu.current_date.month,
            day=self.simu.current_date.day,
            min_date=self.simu.start_date,
            max_date=e,
            title_input="Entrez une date",
            title="Choisissez une date",
        )

        date_dialog.bind(on_save=self.on_save, on_cancel=self.on_cancel)
        date_dialog.open()

    def error(self, title: str, text: str):
        MDDialog(
            title=title,
            text=text,
            buttons=[
                MDFlatButton(
                    text="OK",
                    theme_text_color="Custom",
                    text_color=self.theme_cls.primary_color,
                    on_release=lambda s: s.parent.parent.parent.parent.dismiss(),
                ),
            ],
        ).open()
