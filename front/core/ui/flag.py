from __future__ import annotations
import os, threading
from threading import Thread
import concurrent.futures as cf
from typing import Iterator

from kivy.clock import Clock

from .data import AppData
from kivy.event import EventDispatcher
from kivy.properties import StringProperty, NumericProperty

from .flags.Flag import Flag
from ..data import Country

__all__ = (
    "CountryFlag",
)


class CountryFlag(AppData):
    flag_path = StringProperty(None, allownone=True)
    cid = NumericProperty(None, allownone=True)
    country_name = StringProperty(None, allownone=True)
    ideology = StringProperty(None, allownone=True)

    names = {
        "communism": "Communisme",
        "socialism": "Neutre",
        "left_wing_democracy": "Neutre",
        "right_wing_democracy": "Neutre",
        "fascism": "Totalitarisme",
        "authoritarianism": "Totalitarisme",
        "despotism": "Monarchie",
    }

    PLACEHOLDER = os.path.dirname(__file__) + "/flags/pl.png"

    All: dict[int, CountryFlag] = {}

    pool = cf.ThreadPoolExecutor(max_workers=1)

    @classmethod
    def load(cls):
        for c in Country:
            c: Country
            cls.All[c.id] = cls(cid=c.id, name=c.name)

    @classmethod
    def get(cls, cid: int) -> CountryFlag:
        return cls.All[cid]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.All[self.cid] = self
        self._flag = None
        # self._thread = []

    def on_world(self, *_):
        i = self.world.states[self.cid].ideology.get(self.names)
        if i != self.ideology: self.ideology = i

    def on_ideology(self, *_):
        self.pool.submit(self._get_flag)
        # self._thread.append(Thread(target=self._get_flag))
        # self._thread[-1].start()

    def on_flag_path(self, *_):
        pass

    @property
    def norm_name(self) -> str:
        s = self.country_name
        s = s.replace("É", "E")
        s = s.replace("é", "e")
        s = s.replace("è", "e")
        s = s.replace("ê", "e")
        s = s.replace("ë", "e")
        s = s.replace("ç", "c")
        s = s.replace("à", "a")
        s = s.replace("î", "i")
        s = s.replace("ï", "i")
        s = s.replace("Î", "I")
        s = s.replace("ô", "o")
        return s

    def _get_flag(self, *_):
        Clock.schedule_once(lambda *_: setattr(self, "flag_path", self.PLACEHOLDER), -1)
        # x = threading.current_thread()
        # for i in self._thread.copy():
        #     if i is x: continue
        #     i.join()
        self._flag = Flag(self.norm_name, self.ideology)
        # if self._thread[-1] is x:
        # y=r"C:\Users\Romain\Documents\Python\rire-empire\front\core\ui\flags\fl.png"
        # Clock.schedule_once(lambda *_: setattr(self, "flag_path", y), -1)  # self._flag.path
        Clock.schedule_once(lambda *_: setattr(self, "flag_path", self._flag.path), -1)
        # self._thread.remove(x)

# CountryFlag.load()
