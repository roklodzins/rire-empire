
from ..data import Simulation, Country, CountryState, WorldState
from kivy.app import App
from kivy.event import EventDispatcher
from kivy.properties import NumericProperty, ObjectProperty

__all__ = (
    "AppData",
)


class AppData(EventDispatcher):
    """AppData allow injecting, through Kivy, dependencies & data into inherited widgets.
    Data are read-only excepts selected_country which affects the whole App"""

    selected_country: int = NumericProperty(None, allownone=True)
    country_data: CountryState = ObjectProperty(None, allownone=True)
    country: Country = ObjectProperty(None, allownone=True)
    world: WorldState = ObjectProperty(None, allownone=True)
    simu: Simulation = ObjectProperty(None)

    def on_selected_country(self, *_):
        """Update all data at the App level"""
        App.get_running_app().selected_country = self.selected_country

    def update_state(self, *_):
        """Update all data at the App level"""
        App.get_running_app().update_state()
