from kivy.animation import Animation

from .data import AppData
from kivy.properties import StringProperty, ObjectProperty, NumericProperty
from kivymd.uix.boxlayout import MDBoxLayout

__all__ = (
    "BreakingNews"
)


class BreakingNews(MDBoxLayout, AppData):
    title = StringProperty("Aucune nouvelle")
    content = StringProperty("Lancez une simulation pour voir les nouvelles")
    lce = ObjectProperty([])
    idx = NumericProperty(0)
    ct_opacity = NumericProperty(1)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._anim = None

    def on_world(self, *_):
        self.title = f"Tension mondiale aujourd'hui: {self.world.tension} %"
        self.lce = self.world.lce

    def _show(self, *_):
        if not self.lce:
            self.content = "Aucune nouvelle aujourd'hui"
            self._anim = Animation(ct_opacity=1, d=0.2)
            self._anim.start(self)
        else:
            self.content = self.lce[self.idx]
            self.idx += 1
            if self.idx >= len(self.lce):
                self.idx = 0
            self._anim = Animation(ct_opacity=1, d=0.2)
            if len(self.lce) > 1: self._anim.bind(on_complete=self._show)
            self._anim.start(self)

    def on_lce(self, *_):
        self.idx = 0
        if self._anim:
            self._anim.cancel(self)
        self._anim = Animation(ct_opacity=0, d=0.2)
        self._anim.bind(on_complete=self._show)
        self._anim.start(self)
