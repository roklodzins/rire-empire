
from ..data import WorldState
from .data import AppData
from kivy.properties import ObjectProperty
from kivy_garden.graph import Graph, MeshLinePlot, SmoothLinePlot


class Precision(str):

    @staticmethod
    def norm(s: str) -> str:
        s = s[::-1]
        return " ".join(s[i:i+3] for i in range(0, len(s), 3))[::-1]

    def __mod__(self, u):
        return self.norm(str(int(u))) if int(u) == u else f"{u:.2f}"


class DataGraph(Graph, AppData):
    X = "X"
    Y = "Y"
    Prop = "id"

    def __init__(self, **kwargs):
        super().__init__(**kwargs, xlabel=self.X, ylabel=self.Y, precision=Precision(),
                         x_ticks_major=10,
                         x_ticks_minor=5,
                         y_ticks_minor=2,
                         y_grid_label=True, x_grid_label=True, padding=5, border_color=(.4, .4, .4, 1),
                         x_grid=True, y_grid=True, xmin=-0, xmax=100, ymin=-1, ymax=1)
        self.plot = MeshLinePlot(color=[0, 0.7, 0.5, 1])
        self.add_plot(self.plot)

    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            return True
        return False

    def on_country_data(self, *_):
        self._upd()

    def on_selected_country(self, *_):
        super().on_selected_country()
        self._upd()

    def _upd(self):
        if self.country_data is None or self.selected_country is None:
            return False

        n, a = self.simu.get_around_now(self.selected_country, dates=30)
        p = self.plot.points = [(n + 1, getattr(i, self.Prop)) for n, i in enumerate(a)]
        m = min(i[1] for i in p)
        M = max(i[1] for i in p)
        if m == M:
            m -= 1
            M += 1
        self.ymin = m
        self.ymax = M
        self.xmin = 0
        self.xmax = max(len(p), self.xmin + 1)
        self.y_ticks_major = max(int((M - m) / 5), 2)


class PopulationGraph(DataGraph):
    X = "Jours"
    Y = "Populations"
    Prop = "population"


class GDPGraph(DataGraph):
    X = "Jours"
    Y = "PIB en $"
    Prop = "gdp"

