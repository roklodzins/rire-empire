from kivy.properties import ObjectProperty, StringProperty, NumericProperty, AliasProperty, BooleanProperty, \
    ColorProperty, Clock
from kivy.uix.image import AsyncImage
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.button import MDRectangleFlatIconButton, MDIconButton
from kivymd.uix.expansionpanel import MDExpansionPanel, MDExpansionPanelThreeLine, MDExpansionPanelTwoLine
from kivymd.uix.label import MDIcon
from kivymd.uix.list import TwoLineRightIconListItem, IconLeftWidget, TwoLineIconListItem, ImageLeftWidgetWithoutTouch, \
    BaseListItem
from kivymd.uix.navigationdrawer import MDNavigationDrawer
from kivymd.uix.screen import MDScreen

from ..data import Country, Frontier, Alliance, War
from .data import AppData
from .flag import CountryFlag

__all__ = (
    "RailButton",
    "Rail",
    "CountryItem",
    "NavTitle",
    "Nav",
    "InfoScreen",
)


class ListLine(BaseListItem):
    lbox = ObjectProperty(None, allownone=True)
    rbox = ObjectProperty(None, allownone=True)

    def on_kv_post(self, base_widget):
        self.lbox = self.ids._left_container
        self.rbox = self.ids._right_container


class FlagIcon(AsyncImage):
    pass


class ListLineCountry(ListLine):
    flag = StringProperty(None, allownone=True)
    icon = StringProperty(None, allownone=True)
    icon_color = ColorProperty((1, 1, 1, 1))
    flag_widget = ObjectProperty(None, allownone=True)
    icon_widget = ObjectProperty(None, allownone=True)

    def on_kv_post(self, base_widget):
        super().on_kv_post(base_widget)
        self.flag_widget = FlagIcon(source=self.flag or CountryFlag.PLACEHOLDER)
        self.rbox.add_widget(self.flag_widget)

    def on_flag(self, *_):
        if self.flag_widget is None: return
        self.flag_widget.source = self.flag or CountryFlag.PLACEHOLDER

    def on_icon_color(self, *_):
        if self.icon_widget is None: return
        self.icon_widget.text_color = self.icon_color

    def on_icon(self, *_):
        if self.icon_widget is None:
            if self.icon is None: return
            self.icon_widget = MDIcon(icon=self.icon,
                                      theme_text_color="Custom",
                                      text_color=self.icon_color,
                                      size_hint=(None, None),
                                      pos_hint={"center_y": .5, "center_x": .5},
                                      padding=(self._txt_left_pad, 0, 0, 0))
            if self.lbox: self.lbox.add_widget(self.icon_widget)
        else:
            self.icon_widget.icon = self.icon

    def on_lbox(self, *_):
        if self.icon_widget is None or self.icon_widget in self.lbox.children: return
        self.lbox.add_widget(self.icon_widget)


class RailButton(MDIconButton):
    name = StringProperty("")

    def on_release(self):
        self.parent.nav.sm.current = self.name.lower()
        self.parent.nav.set_state("open")


class Rail(MDBoxLayout):
    nav = ObjectProperty(None)


class CountryItem(CountryFlag, ListLineCountry):
    self_country = ObjectProperty(None)
    nav = ObjectProperty(None)

    def __init__(self, **kwargs):
        super().__init__(cid=kwargs["self_country"].id, country_name=kwargs["self_country"].name, **kwargs)
        # Clock.schedule_once(lambda *_: setattr(self, "world", print("CC") or 42), 2)

    def on_release(self):
        self.nav.sm.current = "stats"
        self.nav.selected_country = self.self_country.id


class NavTitle(MDBoxLayout):
    text = StringProperty("")
    icon = StringProperty("information-outline")
    nav = ObjectProperty(None)


class Nav(MDNavigationDrawer, AppData):
    sm = ObjectProperty(None)
    list = ObjectProperty(None)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._list = [CountryItem(self_country=i, nav=self) for i in Country]
        self._list.sort(key=lambda i: i.self_country.name)

    def on_kv_post(self, base_widget):
        for i in self._list: self.list.add_widget(i)

    def search(self, name):
        self.list.clear_widgets()
        name = name.lower()
        for i in self._list:
            if not name or any(u.startswith(name) for u in i.self_country.keywords):
                self.list.add_widget(i)


class InfoScreen(MDScreen, AppData):
    nav = ObjectProperty(None)
    flag_path = StringProperty(None, allownone=True)
    flagmgr = ObjectProperty(CountryFlag)

    @staticmethod
    def norm(s: str) -> str:
        s = s[::-1]
        return " ".join(s[i:i + 3] for i in range(0, len(s), 3))[::-1]


#############################################################################################################
#############################################################################################################

class AllianceCountryItem(ListLineCountry):
    country = ObjectProperty(None)
    nav = ObjectProperty(None)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        x = CountryFlag.get(self.country.id)
        if x.flag_path != self.flag:
            self.flag = x.flag_path
        x.bind(flag_path=lambda i, v: setattr(self, "flag", v))

    def on_release(self):
        self.nav.selected_country = self.country.id
        self.nav.sm.current = "stats"

    def on_country(self, *_):
        self.text = self.country.name if self.country else ""


class AlliancePanelLine(MDExpansionPanelTwoLine):
    alliance: Alliance = ObjectProperty(None)


class AlliancePanel(MDExpansionPanel):
    nav = ObjectProperty(None)
    alliance: Alliance = ObjectProperty(None)

    def __init__(self, **kwargs):
        self.ct = []
        self.icon_widget = None
        super().__init__(icon="circle-double",
                         content=MDBoxLayout(orientation="vertical", adaptive_height=True),
                         panel_cls=AlliancePanelLine(alliance=self.alliance), **kwargs)

        for i in self.panel_cls.ids._left_container.children:
            if isinstance(i, IconLeftWidget):
                self.icon_widget = i
                break

        if self.icon_widget is not None:
            self.icon_widget.theme_text_color = "Custom"
            self.icon_widget.theme_icon_color = "Custom"
            if self.alliance: self.icon_widget.icon_color = self.alliance.color

    def on_alliance(self, *_):
        self.panel_cls.alliance = self.alliance
        if self.icon_widget is not None:
            if self.alliance: self.icon_widget.icon_color = self.alliance.color

        n = len(self.ct)

        for n, i in enumerate(self.alliance):
            try:
                p = self.ct[n]
                p.country = i
            except:
                p = AllianceCountryItem(country=i, nav=self.nav)
                self.ct.append(p)
                self.content.add_widget(p)

        for i in self.ct[n + 1:]:
            self.content.remove_widget(i)
        del self.ct[n + 1:]


class AllianceScreen(MDScreen, AppData):
    nav = ObjectProperty(None)
    enable_alliance = BooleanProperty(False)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.panels = []
        self.require_update = True

    def on_world(self, *_):
        self.require_update = True
        if self.transition_state == "in":
            self.update_state()

    def on_pre_enter(self, *args):
        if not self.require_update: return
        self.update_state()

    def update_state(self, *_):
        if not self.require_update: return

        if self.world is None:
            self.panels.clear()
            self.ids.list.clear_widgets()
            return

        n = len(self.panels)

        for n, i in enumerate(self.world.alliances):
            try:
                p = self.panels[n]
                p.alliance = i
            except:
                p = AlliancePanel(nav=self.nav, alliance=i)
                self.panels.append(p)
                self.ids.list.add_widget(p)

        for i in self.panels[n + 1:]:
            self.ids.list.remove_widget(i)
        del self.panels[n + 1:]

        self.require_update = False


#############################################################################################################
#############################################################################################################


class WarPanelLine(MDExpansionPanelThreeLine):
    war: War = ObjectProperty(None)


class WarCountryItem(AllianceCountryItem):
    icon_color = ColorProperty((1, 1, 1, 1))
    attack = BooleanProperty(False)


class WarPanel(MDExpansionPanel):
    nav = ObjectProperty(None)
    screen = ObjectProperty(None)
    num = NumericProperty(0)
    war: War = ObjectProperty(None)

    def __init__(self, **kwargs):
        self.ct = []
        super().__init__(content=MDBoxLayout(orientation="vertical", adaptive_height=True),
                         panel_cls=WarPanelLine(war=self.war), **kwargs)

    def on_open(self, *_):
        self.screen.selected_war = self.num

    def on_close(self, *_):
        self.screen.selected_war = None

    def on_war(self, *_):
        self.panel_cls.war = self.war

        n = len(self.ct)
        m = 0

        for side in self.war:
            for n, i in enumerate(side):
                try:
                    p = self.ct[n+m]
                    p.country = i
                    p.attack = not m
                except:
                    p = WarCountryItem(country=i, attack=not m, nav=self.nav)
                    self.ct.append(p)
                    self.content.add_widget(p)
            m += n + 1

        for i in self.ct[m:]:
            self.content.remove_widget(i)
        del self.ct[m:]


class WarScreen(MDScreen, AppData):
    nav = ObjectProperty(None)
    selected_war = NumericProperty(None, allownone=True)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.panels = []
        self.require_update = True

    def on_world(self, *_):
        self.require_update = True
        if self.transition_state == "in":
            self.update_state()

    def on_pre_enter(self, *args):
        if not self.require_update: return
        self.update_state()

    def update_state(self, *_):
        self.selected_war = None
        n = len(self.panels)

        if self.world is None:
            self.panels.clear()
            self.ids.list.clear_widgets()
            return

        for n, i in enumerate(self.world.wars):
            try:
                p = self.panels[n]
                p.num = n
                p.war = i
            except:
                p = WarPanel(nav=self.nav, screen=self, num=n, war=i)
                self.panels.append(p)
                self.ids.list.add_widget(p)

        for i in self.panels[n + 1:]:
            self.ids.list.remove_widget(i)
        del self.panels[n + 1:]

        self.require_update = False
