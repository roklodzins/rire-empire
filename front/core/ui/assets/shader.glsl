/* shader.glsl

simple diffuse lighting based on laberts cosine law; see e.g.:
    http://en.wikipedia.org/wiki/Lambertian_reflectance
    http://en.wikipedia.org/wiki/Lambert%27s_cosine_law
*/
---VERTEX SHADER-------------------------------------------------------
#ifdef GL_ES
    precision highp float;
#endif

attribute vec3  v_pos;
attribute vec3  v_normal;
attribute vec2  v_tc0; // Texture coordinates

uniform mat4 modelview_mat;
uniform mat4 projection_mat;

varying vec4 normal_vec;
varying vec4 vertex_pos;
varying vec2 tex_coords; // Added varying variable for texture coordinates


void main (void) {
    //compute vertex position in eye_space and normalize normal vector
    vec4 pos = modelview_mat * vec4(v_pos,1.0);
    vertex_pos = pos;
    normal_vec = vec4(v_normal,0.0);
    tex_coords = v_tc0;
    gl_Position = projection_mat * pos;
}


---FRAGMENT SHADER-----------------------------------------------------
#ifdef GL_ES
    precision highp float;
#endif

varying vec4 normal_vec;
varying vec4 vertex_pos;
varying vec2 tex_coords; // Texture coordinates

uniform mat4 normal_mat;
uniform sampler2D texture0; // Declare a uniform for the texture

uniform vec3 light_pos;
uniform float light_intensity;
uniform float Ns;
uniform vec3 Ks;
uniform vec3 Ka;
uniform vec3 Kd;

void main (void){
    //correct normal, and compute light vector
    vec4 v_normal = normalize( normal_mat * normal_vec ) ;
    vec4 v_light = normalize( vec4(light_pos,1) - vertex_pos );

    //reflectance based on lamberts law of cosine
    float theta = clamp(dot(v_normal, v_light), 0.0, 1.0);
    //if (theta < 0.03) theta = 0.03;

    vec3 diffuse = theta * Kd;


    vec3 viewDir = normalize(-vertex_pos.xyz);
    vec4 reflectDir = reflect(-v_light, v_normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), Ns);
    vec3 specular = spec * Ks;

    vec3 result = Ka + diffuse + specular; // Sample the texture
    vec4 texture_color = texture2D(texture0, tex_coords); // Sample the texture
    gl_FragColor = vec4(result, 1.0) * texture_color;
}
