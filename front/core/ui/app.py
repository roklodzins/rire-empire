from kivy import Config

Config.set("graphics", "resizable", "1")
Config.set("graphics", "width", "1920")
Config.set("graphics", "height", "1080")

from kivy.logger import Logger
Logger.setLevel("INFO")

from kivy.properties import StringProperty, NumericProperty, ObjectProperty
from kivymd.app import MDApp as App
from kivy.lang import Builder
from .earth import Earth
from .breakingnews import BreakingNews
from .nav import Nav, Rail, RailButton
from .info import IdeologyBar
from .plot import DataGraph
from .date import DateTimeButton
from ..data import Country, Simulation, CountryState, WorldState
from os.path import dirname


class SimuApp(App):

    selected_country: int = NumericProperty(None, allownone=True)
    country_data: CountryState = ObjectProperty(None, allownone=True)
    country: Country = ObjectProperty(None, allownone=True)
    world: WorldState = ObjectProperty(None, allownone=True)
    simu: Simulation = ObjectProperty(None)

    def build(self):
        self.simu = Simulation()
        self.title = "Rire & Empire"
        self.theme_cls.theme_style = "Dark"
        self.theme_cls.primary_palette = "Teal"
        Builder.load_file(dirname(__file__) + "/layout/components.kv", rulesonly=True)
        return Builder.load_file(dirname(__file__) + "/layout/app.kv")

    def on_selected_country(self, w, country):
        self.country = None if country is None else Country[country]
        self.country_data = None if country is None else  self.simu[country]

    def update_state(self):
        self.country_data = None if self.country is None else self.simu[self.country]
        self.world = self.simu.current_world


