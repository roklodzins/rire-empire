import json
import os, random
import shutil
from datetime import datetime, timedelta, date
from os.path import isfile
from subprocess import Popen

from .country import *

__all__ = (
    "Simulation",
    "WorldState",
    "CountryState",
    "Plan",
    "War",
    "Alliance",
    "Ideology",
)


class Ideology:
    __slots__ = (
        "communism",
        "socialism",
        "left_wing_democracy",
        "right_wing_democracy",
        "fascism",
        "authoritarianism",
        "despotism",
    )

    names = {
        "communism": "Communisme",
        "socialism": "Socialisme",
        "left_wing_democracy": "Démocratie de gauche",
        "right_wing_democracy": "Démocratie de droite",
        "fascism": "Fascisme",
        "authoritarianism": "Autoritarisme",
        "despotism": "Despotisme",
    }

    def __init__(self, d: dict):
        self.communism = d["C"]
        self.socialism = d["S"]
        self.left_wing_democracy = d["L"]
        self.right_wing_democracy = d["R"]
        self.authoritarianism = d["A"]
        self.despotism = d["D"]
        self.fascism = d["F"]

    def get(self, names: dict[str, str] = None) -> str:
        x = max(self.__slots__, key=lambda x: getattr(self, x))
        return (names or self.names)[x]

    def __iter__(self):
        return iter(getattr(self, i) for i in self.__slots__)


class Plan:
    __slots__ = (
        "id",
        "day",
        "duration",
    )

    def __init__(self, d: dict):
        self.id = d["I"]
        self.day = d["P"]
        self.duration = d["T"]

    @property
    def finished(self):
        return self.day >= self.duration

    @property
    def progress(self):
        return self.day / self.duration


class CountryState:
    __slots__ = (
        "id",
        "prod_rate",
        "edu_rate",
        "health_rate",
        "industrial_power",
        "agricultural_power",
        "tertiary_power",
        "current_plan",
        "gdp",
        "gdp_growthrate",
        "expenses",
        "incomes",
        "debt",
        "treasury",
        "military_pact",
        "military",
        "victory_points",
        "current_victory_points",
        "population",
        "population_growthrate",
        "population_deathrate",
        "population_density",
        "ideology",
    )

    def __init__(self, d: dict):
        self.id: str = d["Code"]
        self.prod_rate   : float = round(d["PR"], 2)
        self.edu_rate    : float = round(d["E"], 2)
        self.health_rate : float = round(d["H"], 2)

        self.industrial_power   : int = d["IP"]
        self.agricultural_power : int = d["AP"]
        self.tertiary_power     : int = d["TP"]

        self.current_plan: dict = d["CP"]

        self.gdp           : float = round(d["GDP"], 2)
        self.gdp_growthrate: float = round(d["GDPG"], 2)

        self.expenses: float = d["EX"]
        self.incomes : float = d["IN"]
        self.debt    : float = d["DE"]
        self.treasury: float = d["TR"]

        self.military_pact: int = d["MP"]
        self.military     : float = d["M"]

        self.victory_points      : int = d["VP"]
        self.current_victory_points: int = d["CVP"]

        self.population: int = int(d["P"])
        self.population_growthrate: float = round(d["PG"], 2)
        self.population_deathrate : float = round(d["PDR"], 2)
        self.population_density   : float = round(d["PD"], 2)

        self.ideology: Ideology = Ideology(d["I"])

    @property
    def country(self) -> Country:
        return Country[self.id]


class War:

    __slots__ = (
        "attackers",
        "defenders",
        "start_date",
        "time",
    )

    def __init__(self, d: dict):
        self.attackers = tuple(d["A"])
        self.defenders = tuple(d["D"])
        self.start_date = datetime.fromisoformat(d["S"])
        self.time = d["T"]

    @property
    def attackers_countries(self):
        return (Country[i] for i in self.attackers)

    @property
    def defenders_countries(self):
        return (Country[i] for i in self.defenders)

    def __iter__(self):
        return iter((self.attackers_countries, self.defenders_countries))

    def groups_frontiers_attackers(self):
        return Country.group_frontiers(Country[i] for i in self.attackers)

    def groups_frontiers_defenders(self):
        return Country.group_frontiers(Country[i] for i in self.defenders)


class Alliance:

    __slots__ = (
        "name",
        "members",
        "color",
    )

    COLOR = [
        (7 / 255, 154 / 255, 235 / 255),
        (235 / 255, 51 / 255, 27 / 255),
        (235 / 255, 76 / 255, 28 / 255),
        (235 / 255, 180 / 255, 14 / 255),
        (0.133, 0.871, 0.212),
        (9 / 255, 6 / 255, 235 / 255),
        (84 / 255, 6 / 255, 235 / 255),
        (0.133, 0.871, 0.722),
        (128 / 255, 9 / 255, 235 / 255),
        (0.871, 0.133, 0.537),
    ]

    def __init__(self, d: dict, n):
        self.name: str = d["N"]
        self.members = tuple(d["M"])
        try:
            self.color = self.COLOR[n]
        except IndexError:
            self.color = (random.random(), random.random(), random.random())
            self.COLOR.append(self.color)


    def __iter__(self):
        return iter(Country[i] for i in self.members)

    def groups_frontiers(self):
        return Country.group_frontiers(Country[i] for i in self.members)


class WorldState:

    __slots__ = (
        "tension",
        "states",
        "wars",
        "alliances",
        "lce",
    )

    def __init__(self, file):
        d = json.load(file)
        self.tension = round(d["WorldTension"], 2)
        # self.date = date.fromisoformat(d["CurrentDate"])
        self.states = {i["Code"]: CountryState(i) for i in d["Nations"] if i["Code"] in Country}
        self.wars = tuple(War(i) for i in d["Wars"])
        self.alliances = tuple(Alliance(i, n) for n, i in enumerate(d["Alliances"]))
        self.lce = tuple(i["D"] for i in d["LCE"])


class Simulation:
    __slots__ = (
        "_path",
        "_world",
        "_current_date",
        "_start_date",
        "_init_date",
        "_init_world",

        "_seed",
        "_max_days",
        "_process",
    )

    FOLDER = "../back/"
    EXE = "../back/Escalation.exe"
    CONFIG = "../back/Escalation.dll.config"

    TEMPLATE = f"""<?xml version="1.0" encoding="utf-16"?>
    <configuration>
      <startup>
        <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.7.2" />
      </startup>
      <appSettings>
        <add key="Seed" value="{{}}" />
        <add key="MaxDays" value="{{}}" />
        <add key="Output" value="{{}}" />
      </appSettings>
    </configuration>"""

    @classmethod
    def is_back(cls):
        return isfile(cls.EXE)

    def __init__(self):
        self._path = "../common/world/"
        self._current_date = None  # current simu date
        self._start_date = None  # start simu date (date of _world[0])
        self._init_world = None  # init simu world state
        self._init_date = None  # init simu date
        self._world = []
        self._seed = 0
        self._max_days = 0
        self._process = None

    def start(self, seed: int, max_days: int):
        if not self.is_back(): return False
        self._seed = seed
        self._max_days = max_days
        self._path = f"../common/world/{seed}/"

        os.makedirs(os.path.abspath(self._path), exist_ok=True)

        with os.scandir(self._path) as entries:
            for entry in entries:
                if entry.is_dir() and not entry.is_symlink():
                    shutil.rmtree(entry.path)
                else:
                    os.remove(entry.path)

        conf = self.TEMPLATE.format(seed, max_days, self._path)
        open(self.CONFIG, "w", encoding="utf-16").write(conf)
        self._process = Popen(self.EXE, cwd=self.FOLDER)
        return True

    def init(self):
        for i in os.listdir(self._path):
            if os.path.isfile(self._path + i) and i.endswith(".json"):
                self._start_date = date.fromisoformat(i.rstrip(".json"))
                break

        if self._start_date is None: return False
        self._current_date = self._start_date
        self._init_world = self._load(self._start_date)
        if self._init_world is None:
            self._current_date = self._start_date = None
            return False
        else:
            self._world.append(self._init_world)
        return True

    def _load(self, date: date):
        try:
            with open(self._path + date.strftime("%Y-%m-%d") + ".json", "r", encoding="utf-8") as r:
                return WorldState(r)
        except FileNotFoundError:
            return

    def is_loaded(self, date: date, update: bool = False):
        r = 0 <= (date - self._start_date).days < len(self._world)
        if update and r:
            self._current_date = date
        return r

    def is_loadable(self, date: date):
        e = self.end_loadable_date
        return self._start_date <= date <= e, e

    def load(self, date: date):
        x = (date - self._start_date).days
        m = len(self._world) - 1
        if x < 0: return 0

        if x <= m:
            self._current_date = date
            return 1  # already loaded

        dt = self._current_date

        i = m + 1
        for i in range(m + 1, x + 1):
            dt = self._start_date + timedelta(days=i)
            w = self._load(dt)

            if w is None:  # Failed to load 1 day
                self._current_date = dt
                return i - m - 1

            self._world.append(w)

        self._current_date = dt
        return i - m - 1

    @property
    def start_date(self):
        return self._start_date

    @property
    def end_date(self):
        return self._start_date + timedelta(days=len(self._world) - 1)

    @property
    def end_loadable_date(self):
        r = os.listdir(self._path)
        for i in reversed(r):
            if os.path.isfile(self._path + i) and i.endswith(".json"):
                return date.fromisoformat(i.rstrip(".json"))
        return self._init_date

    @property
    def init_date(self):
        return self._init_date

    @property
    def init_world(self):
        return self._init_world

    @property
    def current_date(self):
        return self._current_date

    @property
    def current_world(self):
        return self._world[(self._current_date - self._start_date).days]

    def __getitem__(self, item: Country | int) -> CountryState:
        return self.current_world.states[item.id if isinstance(item, Country) else item] if self._current_date else None

    def get_until_now(self, item: Country | int) -> tuple[CountryState, ...]:
        u = item.id if isinstance(item, Country) else item
        now = (self._current_date - self._start_date).days
        return tuple(i.states[u] for i in self._world[:now + 1])

    def get_around_now(self, item: Country | int, dates: int = 1) -> tuple[int, tuple[CountryState, ...]]:
        u = item.id if isinstance(item, Country) else item
        e = now = (self._current_date - self._start_date).days + 1
        s = now - dates
        if s < 0:
            e -= s
            s = 0
        return now - 1 - s, tuple(i.states[u] for i in self._world[s:e])
