from __future__ import annotations

from typing import Iterator

from .coord import Coordinate


class MetaFrontier(type):
    All: list[Frontier] = []

    def __len__(cls):
        return len(cls.All)

    def __getitem__(cls, item) -> Frontier:  # Make Territory sliceable
        return cls.All[item]

    def __iter__(cls) -> Iterator[Frontier]:
        return iter(cls.All)


class Frontier(metaclass=MetaFrontier):
    """Represents a fix geographical frontier between two territories"""

    __slots__ = (
        "_id",
        "_coords",
        "_territories",
        "_box",
    )

    def __init__(self, coords: list[Coordinate], territories: list[tuple[int, bool]]):
        self._id = len(x := type(self).All)
        self._coords = coords
        self._territories = territories
        self._box = None  # Coordinate.box(*coords)
        x.append(self)

    @classmethod
    def create(cls, coords: list[Coordinate], territories) -> int:
        return cls(coords, territories).id

    @property
    def id(self): return self._id

    @property
    def coords(self): return self._coords

    @property
    def territories(self): return self._territories

    @property
    def box(self):
        raise NotImplementedError("if needed, uncomment the _box init in the constructor")
        # return self._box

    def __len__(self):
        return len(self._coords)

    def __iter__(self):
        return iter(self._coords)

    def __repr__(self):
        return f"<Frontier {self._id}: {self._territories}>"



