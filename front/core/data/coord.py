from __future__ import annotations
import json
from math import log, tan, pi, radians, sin, cos


class Coordinate:
    __slots__ = (
        "_long",
        "_lat",
        "_2d",
        "_3d"
    )

    def __init__(self, long: float, lat: float):
        self._long = long
        self._lat = lat
        self._2d = self.gps_to_2d(long, lat)
        self._3d = self.gps_to_3d(long, lat)

    @classmethod
    def from_2d(cls, u: float, v: float):
        return cls(*cls.c2d_to_gps(u, v))

    @staticmethod
    def gps_to_mercator(long: float, lat: float) -> tuple[float, float]:
        return (long+180) / 360, log(tan(pi / 4 + radians(lat) / 2)) / (2 * pi)  # [0,1] [-, +]

    @staticmethod
    def gps_to_2d(long: float, lat: float) -> tuple[float, float]:
        return (long + 180) / 360, (lat + 90) / 180  # [0,1] [0,1]

    @staticmethod
    def c2d_to_gps(u: float, v: float) -> tuple[float, float]:
        return u * 360 - 180, v * 180 - 90  # [0,1] [0,1]

    @staticmethod
    def gps_to_3d(long: float, lat: float) -> tuple[float, float, float]:
        phi = radians(lat)
        tho = radians(long)
        s = sin(phi)
        return s * cos(tho), s * sin(tho), cos(phi)

    @property
    def longitude(self): return self._long

    @property
    def latitude(self): return self._lat

    @property
    def coord2d(self): return self._2d

    @property
    def coord3d(self): return self._3d

    def __eq__(self, other):
        return self._long == other.longitude and self._lat == other.latitude

    def __hash__(self):
        return hash((self._long, self._lat))

    def __repr__(self):
        return f"({self._lat}°, {self._long}°)"

    @classmethod
    def box(cls, *coords: Coordinate) -> tuple[Coordinate, Coordinate]:
        return cls(min((i.longitude for i in coords)), min((i.latitude for i in coords))), \
               cls(max((i.longitude for i in coords)), max((i.latitude for i in coords)))


class CoordBox:
    __slots__ = (
        "_min", "_max"
    )

    @property
    def min(self): return self._min

    @property
    def max(self): return self._max

    def __init__(self, cmin: Coordinate, cmax: Coordinate):
        self._min = cmin
        self._max = cmax

    def __contains__(self, item: Coordinate):
        return self._min.longitude <= item.longitude <= self._max.longitude and \
               self._min.latitude <= item.latitude <= self._max.latitude


class Vector:
    __slots__ = (
        "x1", "x2", "y1", "y2"
    )

    def __init__(self, c1: Coordinate, c2: Coordinate):
        self.x1, self.y1 = c1.coord2d
        self.x2, self.y2 = c2.coord2d

    def intersect(self, other: Vector) -> bool:
        a1 = self.y2 - self.y1
        b1 = self.x1 - self.x2
        c1 = (self.x2 * self.y1) - (self.x1 * self.y2)

        d1 = (a1 * other.x1) + (b1 * other.y1) + c1
        d2 = (a1 * other.x2) + (b1 * other.y2) + c1

        if d1 * d2 >= 0: return False

        a2 = other.y2 - other.y1
        b2 = other.x1 - other.x2
        c2 = (other.x2 * other.y1) - (other.x1 * other.y2)

        d1 = (a2 * self.x1) + (b2 * self.y1) + c2
        d2 = (a2 * self.x2) + (b2 * self.y2) + c2

        if d1 * d2 >= 0: return False

        if abs((a1 * b2) - (a2 * b1)) < 1e-5: return False  # return 2  (in & out = 2 = even as 0)
        return True
