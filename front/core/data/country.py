from __future__ import annotations

from typing import Any, Generator, Iterator, Iterable

from .coord import Coordinate
from .territory import Territory
import random, os, json

__all__ = (
    "Country",
)


class MetaCountry(type):
    All: dict[int, Country] = {}

    def __len__(cls):
        return len(cls.All)

    def __getitem__(cls, item) -> Country:  # Make Country sliceable
        return cls.All[item]

    def __contains__(self, item) -> bool:
        return item in self.All

    def __iter__(cls) -> Iterator[Country]:
        return iter(cls.All.values())


class Country(metaclass=MetaCountry):
    """Represents a country"""

    __slots__ = (
        "_id",
        "_territories",
        "_name",
        "_capital",
        "_capcoord",
        "_keywords",
    )

    @classmethod
    def load(cls, countries: str = "../common/countries.json"):

        with open(countries, "r", encoding="utf-8") as r:
            ct = json.load(r)

        for i in ct:
            cls.All[i["id"]] = cls(**i)

    @classmethod
    def get(cls, coord: Coordinate) -> Country:
        for i in cls:
            i: Country
            if coord in i:
                return i

    def __init__(self, id: int, name: str, territories: list[int], capital: str, capcoord: tuple[float, float]):
        self._id = id
        self._name = name
        self._territories = territories
        self._capital = capital
        self._capcoord = Coordinate(*capcoord)

        self._keywords = {u for i in self._name.lower().split(" ") for u in i.split('-')}

    @property
    def id(self) -> int:
        return self._id

    @property
    def name(self) -> str:
        return self._name

    @property
    def keywords(self) -> set[str]:
        return self._keywords

    @property
    def territories(self) -> list[int]:
        return self._territories

    @property
    def capital(self) -> str:
        return self._capital

    @property
    def capcoord(self):
        return self._capcoord

    @property
    def surface(self):
        return sum(i.surface for i in self)

    def __len__(self):
        return len(self._territories)

    def __getitem__(self, idx: int) -> Territory:
        return Territory[self._territories[idx]]

    def __iter__(self) -> Generator[Territory, Any, None]:
        return (Territory[i] for i in self._territories)

    def __contains__(self, item):
        if isinstance(item, Coordinate):
            r = any(map(lambda x: item in x, self))

        elif isinstance(item, int):
            r = item in self._territories

        elif isinstance(item, Territory):
            r = item.id in self._territories

        else: r = False
        # if r: print(coord, "in", self._name)
        return r

    @classmethod
    def group_frontiers(cls, countries: Iterable[Country]) -> tuple:
        """Groups the frontiers of the given countries"""

        return Territory.group_frontiers(i for u in countries for i in u)
