from __future__ import annotations

from itertools import chain
from typing import Iterator, Iterable, Generator, Any
from math import cos, asin, pi, tan, atan, radians

from .coord import Coordinate, Vector
from .frontier import Frontier
import os, json

__all__ = (
    "Territory",
)


class MetaTerritory(type):
    All: list[Territory] = []

    def __len__(cls):
        return len(cls.All)

    def __getitem__(cls, item) -> Territory:  # Make Territory sliceable
        return cls.All[item]

    def __iter__(cls) -> Iterator[Territory]:
        return iter(cls.All)


class Territory(metaclass=MetaTerritory):
    """Represents a fix, independent, geographical territory"""

    __slots__ = (
        "_id",
        "_outer",
        "_outer_frontiers",
        "_inners",
        "_inner_frontiers",
        "_box",
        "_surface",
    )

    @classmethod
    def load(cls, territories: str = "../common/territories.json"):

        with open(territories, "r") as r:
            tr = json.load(r)

        coords = {}
        neighbors = {}

        print("Colliding borders...")
        for n, f in enumerate(tr):
            cls.All.append(T := cls(n, **f))
            outer = T._outer
            inners = T._inners

            # Iters over all borders and caches, keeping the iter order, all coords and group them by shared territory (id)
            # this group id is not unique as A & B can share 2+ differents borders
            o = iter(outer)
            last = next(o)
            neighbors.setdefault(last, set())
            for u in o:
                coords.setdefault(u, []).append((n, True))
                neighbors.setdefault(u, set()).add(last)
                neighbors[last].add(u)
                last = u

            for ins in inners:
                o = iter(ins)
                last = next(o)
                neighbors.setdefault(last, set())
                for u in o:
                    coords.setdefault(u, []).append((n, False))
                    neighbors.setdefault(u, set()).add(last)
                    neighbors[last].add(u)
                    last = u

        print("Sealing collisions...", len(coords))
        for i in coords:
            coords[i] = frozenset(coords[i])  # make it hashable

        print("Compiling frontiers...")  # Build frontiers from coords
        coord, ter = next(iterc := iter(coords.items()))
        frontiers = {(last := ter): [n := [coord]]}

        # frontiers[sharing ter] = list of [frontiers coords]

        for coord, ter in iterc:
            if ter == last and coord in neighbors[n[-1]]:
                n.append(coord)
            else:
                m = frontiers.get(last := ter)
                if m is None:
                    m = frontiers[last] = []
                m.append(n := [coord])

        print("Merging contiguous frontiers...")  # Merge contiguous frontiers from the same sharer territories
        for n, (ter, coords) in enumerate(frontiers.items()):
            while 1:
                try:
                    for A in coords:
                        for B in coords:
                            if A == B: break
                            if A[0] in neighbors[B[-1]]:
                                B.extend(A)
                                A.clear()
                                raise StopIteration
                            if B[0] in neighbors[A[-1]]:
                                A.extend(B)
                                B.clear()
                                raise StopIteration
                            if A[0] in neighbors[B[0]]:
                                B.reverse()
                                B.extend(A)
                                A.clear()
                                raise StopIteration
                            if A[-1] in neighbors[B[-1]]:
                                B.reverse()
                                A.extend(B)
                                B.clear()
                                raise StopIteration
                except StopIteration:
                    while [] in coords: coords.remove([])
                    continue
                break

        print("Extending frontiers...")  # Extend frontiers extremities to the next sharer territories
        for n, (Ater, Acoords) in enumerate(frontiers.items()):
            for Acoord in Acoords:
                if Acoord[0] in neighbors[Acoord[-1]]:
                    Acoord.append(Acoord[0])
                else:
                    if len(Ater) > 2: continue
                    cn = Coordinate(-180, -90)
                    X = Acoord[1] if len(Acoord) > 1 else cn
                    Y = Acoord[-2] if len(Acoord) > 1 else cn
                    Acoord.insert(0, next(i for i in neighbors[Acoord[0]] if i != X))
                    try:
                        Acoord.append(next(i for i in neighbors[Acoord[-1]] if i != Y))
                    except:
                        pass  # print(Y, neighbors[Acoord[-1]])


        # for n, (Ater, Acoords) in enumerate(frontiers.items()):
        #    #if len(Ater) > 2 and any(i for i in Acoords if len(i) == 1): print("ERR", Ater, Acoords)
        #    for Acoord in Acoords:
        #        if Acoord[0] in neighbors[Acoord[-1]]:
        #            Acoord.append(Acoord[0])
        #        else:
        #            for u, (Bter, Bcoords) in enumerate(frontiers.items()):
        #                if u == n: break
        #                try:
        #                    for Bcoord in Bcoords:
        #                        if Acoord[0] in neighbors[Bcoord[0]]:
        #                            Acoord.insert(0, Bcoord[0])
        #                            Bcoord.insert(0, Acoord[0])
        #                            print("Reconstruct", Ater, Bter)
        #                        elif Acoord[0] in neighbors[Bcoord[-1]]:
        #                            Acoord.insert(0, Bcoord[-1])
        #                            Bcoord.append(Acoord[0])
        #                            print("Reconstruct", Ater, Bter)
        #                        if Acoord[-1] in neighbors[Bcoord[0]]:
        #                            Acoord.append(Bcoord[0])
        #                            Bcoord.insert(0, Acoord[-1])
        #                            print("Reconstruct", Ater, Bter)
        #                        elif Acoord[-1] in neighbors[Bcoord[-1]]:
        #                            Acoord.append(Bcoord[-1])
        #                            Bcoord.append(Acoord[-1])
        #                            print("Reconstruct", Ater, Bter)
        #                except StopIteration:
        #                   print("Reconstruct", Ater, Bter)

        print("Extracting frontiers...", len(frontiers))
        for ter, coords in frontiers.items():
            if len(ter) > 2: continue

            fr = {Frontier.create(coord, ter) for coord in coords}

            for tid, side in ter:
                t: Territory = Territory[tid]
                t: set = t._outer_frontiers if side else t._inner_frontiers
                t.update(fr)

        for t in Territory:  # Seal frontiers
            t: Territory
            t._outer_frontiers = frozenset(t._outer_frontiers)
            t._inner_frontiers = frozenset(t._inner_frontiers)

        print("Fontiers loaded")

    @classmethod
    def get(cls, coord: Coordinate) -> Territory:
        for i in cls:
            if coord in i:
                return i

    def __init__(self, id: int, outer: tuple | list, inners: tuple | list, **kw):
        self._id = id
        self._outer: tuple[Coordinate, ...] = tuple(Coordinate(*i) for i in outer)
        self._inners = tuple(tuple(Coordinate(*i) for i in inner) for inner in inners)
        self._box = Coordinate.box(*self._outer)

        self._outer_frontiers: frozenset | set = set()
        self._inner_frontiers: frozenset | set = set()

        self._surface = self._get_surface(self._outer) - sum(map(self._get_surface, self._inners))

    @staticmethod
    def _get_surface(coords: tuple[Coordinate, ...]) -> float:
        """Speherical polygon surface calculation
        Source: https://forum.worldwindcentral.com/forum/archived-forums/worldwind-net-development/developers-corner/10518-a-method-to-compute-the-area-of-a-spherical-polygon?t=20724
        """

        r = 0
        lam1 = radians(coords[0].longitude)
        beta1 = radians(coords[0].latitude)
        cosB1 = cos(beta1)

        for j in range(1, len(coords)):

            lam2 = radians(coords[j].longitude)
            beta2 = radians(coords[j].latitude)
            cosB2 = cos(beta2)

            if lam1 != lam2:
                hav = (1 - cos(beta2 - beta1) + (1 - cos(lam2 - lam1)) * cosB1 * cosB2) / 2
                a = 2 * asin(hav ** .5)
                b = pi / 2 - beta2
                c = pi / 2 - beta1
                s = (a + b + c) / 2
                excess = abs(4 * atan(abs(tan(s / 2) * tan((s - a) / 2) * tan((s - b) / 2) * tan((s - c) / 2)) ** .5))

                r += -excess if lam2 < lam1 else excess

            lam1 = lam2
            beta1 = beta2
            cosB1 = cosB2

        return abs(r) * 6371 ** 2

    @staticmethod
    def _collide_border(coord: Coordinate, border: tuple[Coordinate, ...]) -> bool:  # Ray Casting Algo
        ray = Vector(Coordinate(-180, -90), coord)
        return bool(sum(ray.intersect(Vector(a, border[(i + 1) % len(border)])) for i, a in enumerate(border)) & 1)

    def _collide_box(self, coord: Coordinate) -> bool:
        return self._box[0].longitude <= coord.longitude <= self._box[1].longitude and \
            self._box[0].latitude <= coord.latitude <= self._box[1].latitude

    def collide(self, coord: Coordinate) -> bool:
        return self._collide_box(coord) and self._collide_border(coord, self._outer) and not any(map(lambda x: self._collide_border(coord, x), self._inners))

    @property
    def id(self) -> int:
        return self._id

    @property
    def outer(self) -> tuple[Coordinate, ...]:
        return self._outer

    @property
    def inners(self) -> tuple[tuple[Coordinate, ...], ...]:
        return self._inners

    @property
    def outer_frontiers(self) -> frozenset[Frontier]:
        return self._outer_frontiers

    @property
    def inner_frontiers(self) -> frozenset[Frontier]:
        return self._inner_frontiers

    @property
    def surface(self) -> float:
        return self._surface

    def __contains__(self, coord: Coordinate) -> bool:
        a = self.collide(coord)
        # if a:
            # print("collide", coord)
            # print("Ter", self.id, "\tcollide", coord)
            # for i in self:
            #     print(i.id, "\t:", len(i))
        return a

    def __iter__(self):
        return map(Frontier.__getitem__, self._outer_frontiers | self._inner_frontiers)

    @classmethod
    def group_frontiers(cls, frontiers: Iterable[Frontier] | Generator[Territory, Any, None]) -> tuple[frozenset[Frontier], frozenset[Frontier]]:
        d = {}
        for i in frontiers:
            for j in i:
                d.setdefault(j, []).append(i)

        i, o = [], []
        for f, t in d.items():
            (o if len(t) == 1 else i).append(f)

        return frozenset(i), frozenset(o)
