from core import SimuApp


def run():
    SimuApp().run()


if __name__ == '__main__':
    run()
