import json

d = json.loads(open("CShapes-2.0.geojson", "r").read())
d = d["features"]

ter = []
dter = []
ct = {}
cth = {}
hs = set()

ERR_FIX = 0

for i in d:
    s = i["properties"]["gwsyear"]
    e = i["properties"]["gweyear"]
    if s <= 1966 <= e:
        n = i["properties"]["cntry_name"]
        D = False  # n == "Mali"
        if D and not ERR_FIX:
            ERR_FIX = 1
            continue
        x = ct.setdefault(n, [])
        y = cth.setdefault(n, set())
        for u in [i["geometry"]["coordinates"]] if i["geometry"]["type"] == "Polygon" else i["geometry"]["coordinates"]:
            A = tuple(tuple(w) for w in u[0])
            B = tuple(tuple(tuple(w) for w in v) for v in u[1:]) if len(u) > 1 else ()

            pA = pB = False

            if hash(A) in hs:
                if pA := (hash(A) in y): print("pass duplicate for", n)
                else: raise Exception("Wtf")
            else:
                hs.add(hash(A))
                y.add(hash(A))
            if B:
                if hash(B) in hs:
                    if pB := (hash(B) in y): print("pass inner duplicate for", n)
                    else: raise Exception("Wtf inner")
                else:
                    hs.add(hash(B))
                    y.add(hash(B))
            else:
                pB = pA

            if pA != pB: raise Exception("Wtf ter")
            elif pA: continue
            x.append(len(ter))
            ter.append({
                "outer":  A,
                "inners": B
            })
            dter.append({
                "population": 1000,
                "adhesion": 1,
                "admin": None,
            })
            if D: print(x, len(u), len(A), len(B))
            if D and len(x) > 1:
                print(">>", ter[x[0]] == ter[x[1]])
c = {
    "center": [
        0,
        0
    ],
    "birth_rate": 0,
    "death_rate": 0,
    "pib": 0,
    "eco_growth": 0,
    "eco_stability": 1,
    "militarization": 0,
    "army_size": 0,
    "army_budget": 0,
    "politiscale": [
        0,
        0
    ],
    "stability": 1,
    "soft_power": 0,
    "admin_by": None,
    "admin_list": []
}
ctr = [{"id": n, "name": m, "territories": i} for n, (m, i) in enumerate(ct.items())]

open("core/data/database/territories.json", "w").write(json.dumps(ter, indent=4))
open("core/data/database/new_territories.json", "w").write(json.dumps(dter, indent=4))
open("core/data/database/base_countries.json", "w").write(json.dumps(ctr, indent=4))
for i in ctr:
    i.update(c)
open("core/data/database/new_countries.json", "w").write(json.dumps(ctr, indent=4))

if __name__ == '__main__':
    print(len(ter))
