# Rire & Empire

# Description

Ce dépôt contient les sources de notre projet d'Architecture Logicielle et Qualité.
Simulation géopolitique en 3D, au jour par jour de la guerre froide.

# Installation

- Build le projet front avec le back (precompiled embedded) (voir `.\dist`)
- Ou lancer le projet compilé dans `Rire & Empire.zip`

# Requirements

- Windows
- Ordinateur supportant OpenGL
- Carte graphique résonnablement récente (supportant les Textures en 20k (~20000px) et la fonction `dot` de `glsl`)
- Python 3.11, ou plus récent (uniquement pour build le projet)

- En cas de problème, nous contacter

# Auteurs

- Guillaume Assailly
- Romain Klodzinski
- Antoine Mura
- Paul Quinzi

# Licence MIT

« Copyright © 2024, author: see above
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders X be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.

Except as contained in this notice, the name of us shall not be used in advertising or otherwise to promote the sale, use or other dealings in this Software without prior written authorization from us. »

