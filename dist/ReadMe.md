# Build the front app

- create a virtual env with only the packages in front's requirements.txt + pyinstaller: `prepare.bat`
- build the bootloader using pyinstaller: `build.bat`
- copy the front code & deps, compiled back é common data to the dist folder: `package.bat`
- start `Rire & Empire.exe` from its folder in `.\exe\Rire & Empire\`
