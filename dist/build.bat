call Scripts\activate
pyinstaller main.py -n "Rire & Empire" --contents-directory front -y --distpath exe
call Scripts\deactivate

xcopy /s /e /y /d Lib\site-packages\kivy\ "exe\Rire & Empire\front\kivy\"
xcopy /s /e /y /d Lib\site-packages\kivymd\ "exe\Rire & Empire\front\kivymd\"
xcopy /s /e /y /d Lib\site-packages\kivy_deps\ "exe\Rire & Empire\front\kivy_deps\"
xcopy /s /e /y /d Lib\site-packages\kivy_garden\ "exe\Rire & Empire\front\kivy_garden\"
xcopy /s /e /y /d Lib\site-packages\kivy3\ "exe\Rire & Empire\front\kivy3\"
xcopy /s /e /y /d Lib\site-packages\garden\ "exe\Rire & Empire\front\garden\"
xcopy /s /e /y /d Lib\site-packages\PIL\ "exe\Rire & Empire\front\PIL\"
xcopy /s /e /y /d share\ "exe\Rire & Empire\front\share\"
xcopy /s /e /y /d back\ "exe\Rire & Empire\back\"
