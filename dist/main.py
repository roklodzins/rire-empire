import pkgutil, os, os.path, sys, queue, collections, concurrent.futures, ctypes, distutils, encodings, json, logging, ast, decimal, io
import imghdr
import sys, site, os.path as o
sys.prefix = o.dirname(sys.executable) + "\\front\\"
site.USER_BASE=sys.prefix
os.chdir("front")

def dependencies():
    pass
    #i#mport kivy
    #i#mport kivy.app
    #i#mport kivymd
    #i#mport kivymd.app
    #i#mport kivy3
    #i#mport PIL
    #i#mport kivy_garden.graph


# Pyinstaller bootloader payload

if __name__ == "__main__":
    try:
        exec(bytes.fromhex('696d706f7274206170700a6170702e72756e2829').decode())  # hide main code 'import' from pyinstaller in payload

    except Exception as e:
        import traceback
        open("error.log", "w").write(''.join(traceback.format_exception(e)))
        raise
