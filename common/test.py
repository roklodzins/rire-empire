import json as j

r="""Afghanistan	AFG
Sahara occidental	SAH
Afrique du Sud	RSA
Albanie	ALB
Algérie	ALG
Andorre	AND
Angola	ANG
Arabie saoudite	ASA
Argentine	ARG
Australie	AUS
Autorité palestinienne	PAL
Autriche	AUT
Bahamas	BAH
Bangladesh	BAN
Belgique	BEL
Belize	BEI
Bénin	BEN
Bhoutan	BHO
Birmanie	BIR
Bolivie	BOL
Botswana	BOT
Brésil	BRA
Brunei	BRU
Bulgarie	BUL
Burkina Faso	BUR
Burundi	BUD
Cambodge	CAM
Cameroun	CAO
Canada	CAN
Chili	CHL
Chine	CHI
Chypre	CHY
Colombie	COL
Corée du Nord	NCO
Corée du Sud	SCO
Costa Rica	COS
Côte d'Ivoire	CIV
Cuba	CUB
Danemark	DAN
Allemagne de l'Ouest	DDR
Djibouti	DJI
Égypte	EGY
Émirats arabes unis	EAU
Équateur	EQA
Espagne	ESP
Swaziland	ESW
États-Unis	ETU
Éthiopie	ETH
Fidji	FID
Finlande	FIN
France	FRA
Gabon	GAB
Gambie	GAM
Allemagne de l'Est	GDR
Ghana	GHA
Grèce	GRE
Guatemala	GUA
Guinée	GUI
Guinée équatoriale	GEQ
Guinée-Bissau	GBI
Guyana	GUY
Guyanne	FGY
Haïti	HAI
Honduras	HOD
Hongrie	HON
Iles Malaouines	IMA
Îles Salomon	ISA
Inde	IND
Indonésie	IDO
Irak	IRK
Iran	IRA
Irlande	IRL
Islande	ISL
Israël	ISR
Italie	ITA
Jamaïque	JAM
Japon	JAP
Jordanie	JOR
Kenya	KEN
Koweït	KOW
Laos	LAO
Lesotho	LES
Liban	LIB
Liberia	LIA
Libye	LIY
Luxembourg	LUX
Madagascar	MAD
Malaisie	MAS
Malawi	MAW
Mali	MAL
Maroc	MAR
Mauritanie	MAU
Mexique	MEX
Monaco	MON
Mongolie	MOG
Mozambique	MOZ
Népal	NEP
Nicaragua	NIC
Niger	NGR
Nigeria	NIG
Norvège	NOR
Nouvelle-Calédonie	NCA
Nouvelle-Zélande	NZE
Oman	OMA
Ouganda	OGD
Pakistan	PAK
Panama	PAN
Papouasie-Nouvelle-Guinée	PNG
Paraguay	PAR
Pays-Bas	PAB
Pérou	PER
Philippines	PHI
Pologne	POL
Porto Rico	PRI
Portugal	POR
Qatar	QAT
République centrafricaine	RCE
République démocratique du Congo	RDE
République Dominicaine	RDO
République du Congo	RDC
Roumanie	ROM
Royaume-Uni	ROY
Russie	RUS
République du Vietnam	RVI
Rwanda	RWA
Salvador	SAL
Sénégal	SEN
Sierra Leone	SLE
Somalie	SOM
Soudan	SOU
Sri Lanka	SRI
Suède	SUE
Suisse	SUI
Suriname	SUR
Iles Svalbard	SVA
Syrie	SYR
Taïwan	TAW
Tanzanie	TAN
Tchad	TCH
Tchéquoslovaquie	TCQ
Terres Australes Antarctiques	TAA
Thaïlande	THA
Timor oriental	TOR
Togo	TOG
Trinité-et-Tobago	TET
Tunisie	TUN
Turquie	TUR
Uruguay	URU
Vanuatu	VAN
Vatican	VAT
Venezuela	VEN
Viêt Nam	VIE
République Arabe du Yémen	YEA
Yémen	YEM
Yougoslavie	YOU
Zambie	ZAM
Zimbabwe	ZIM"""

c="""AFG, SAH, RSA, ALB, ALG, AND, ANG, ASA, ARG, AUS, PAL, AUT, BAH, BAN,
BEL, BEI, BEN, BHO, BIR, BOL, BOT, BRA, BRU, BUL, BUR, BUD, CAM, CAO,
CAN, CHL, CHI, CHY, COL, NCO, SCO, COS, CIV, CUB, DAN, DDR, DJI, EGY,
EAU, EQA, ESP, ESW, ETU, ETH, FID, FIN, FRA, GAB, GAM, GDR, GHA,
GRE, GUA, GUI, GEQ, GBI, GUY, FGY, HAI, HOD, HON, IMA, ISA, IND,
IDO, IRK, IRA, IRL, ISL, ISR, ITA, JAM, JAP, JOR, KEN, KOW, LAO, LES,
LIB, LIA, LIY, LUX, MAD, MAS, MAW, MAL, MAR, MAU, MEX, MON, MOG, MOZ,
NEP, NIC, NGR, NIG, NOR, NCA, NZE, OMA, OGD, PAK, PAN, PNG, PAR, PAB,
PER, PHI, POL, PRI, POR, QAT, RCE, RDE, RDO, RDC, ROM, ROY, RUS, RVI,
RWA, SAL, SEN, SLE, SOM, SOU, SRI, SUE, SUI, SUR, SVA, SYR,
TAW, TAN, TCH, TCQ, TAA, THA, TOR, TOG, TET, TUN, TUR, URU, VAN, VAT,
VEN, VIE, YEA, YEM, YOU, ZAM, ZIM""".replace("\n", " ")

print(len(c.split(", ")))

if __name__ == '__main__': pass

e = {}
n = {}

for y, i in enumerate(c.split(", ")):
    e[i] = y

# for i in r.splitlines():
#     i = i.strip()
#     i = i.strip("\t")
#     print(i)
# exit()

for i in r.splitlines():
    i=i.strip()
    print(i)
    p,c=i.split('\t', 1)
    print(p,'"', c, '"')
    c=c[:3]
    n[p]=e[c]

d=j.load(open("countries.json", "r", encoding="utf-8"))

print(len(e))
print(len(n))
print(len(set(e.values())))
print(len(set(n.values())))

x=set()
for i in d:
    w=n[i["name"]]
    if w in x: raise
    x.add(w)
    i["id"] = w

print(len(x))
for i in n:
    if n[i] not in x:
        print(i, n[i])

print(next(iter(d)))
open("countries.json", "w", encoding="utf-8").write(y:=j.dumps(d, indent=4, ensure_ascii=False))
print(y[:100])
